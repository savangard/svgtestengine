package svg.test.engine.assertion;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.SOAPBlock;
import com.softwareag.is.datetime.DateTimeUtil;
import com.softwareag.util.IDataMap;
import com.wm.data.IData;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Provider;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Arrays;
// --- <<IS-END-IMPORTS>> ---

public final class dev

{
	// ---( internal utility methods )---

	final static dev _instance = new dev();

	static dev _newInstance() { return new dev(); }

	static dev _cast(Object o) { return (dev)o; }

	// ---( server methods )---




	public static final void nullAppender (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(nullAppender)>> ---
		// @sigtype java 3.5
		// [i] field:0:required fieldPath
		// [i] field:1:required fieldNames
		// [i] object:0:required substituteIfNullText
		// [i] object:0:required substituteOnlyWhenEmpty
		try {
			IDataMap pipelineMap = new IDataMap(pipeline);
			String fieldPath = pipelineMap.getAsNonEmptyString("fieldPath");
			Boolean substituteIfNullText = pipelineMap.getAsBoolean("substituteIfNullText", false);
			Boolean substituteOnlyWhenEmpty = pipelineMap.getAsBoolean("substituteOnlyWhenEmpty", false);
			if (!fieldPath.contains("/")) {
				pipelineMap.put(fieldPath, null);
			} else {
				List<String> pathElements = Arrays.asList(fieldPath.split("/"));
				String root = pathElements.get(0);
				IDataMap dataMap = getAsIDataMap(pipelineMap, root);
				String fieldName = pathElements.get(pathElements.size()-1 );
				for (int i=1 ; i < pathElements.size() -1; i++) {
					dataMap = getAsIDataMap(dataMap, pathElements.get(i));
				}
				String[] arr = pipelineMap.getAsStringArray("fieldNames");
		//				List<String> fieldNames = Arrays.asList();
				if (arr != null && arr.length > 0) {
					for (String f : arr) {
						if (substituteIfNullText) {
							String data = dataMap.getAsNonEmptyString(f, "");
							if ("null".equals(data)) {
								dataMap.put(f, null);
							}
						} else if (substituteOnlyWhenEmpty) {
							if (!dataMap.containsKey(f)) {
								dataMap.put(f, null);
							}
						} else {
							dataMap.put(f, null);
						}
					}
				} else {
					if (substituteIfNullText) {
						String data = dataMap.getAsNonEmptyString(fieldName, "");
						if ("null".equals(data)) {
							dataMap.put(fieldName, null);
						}
					} else if (substituteOnlyWhenEmpty) {
						if (!dataMap.containsKey(fieldName)) {
							dataMap.put(fieldName, null);
						}
					} else {
						dataMap.put(fieldName, null);
					}
				}
			}
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	
	private static IDataMap getAsIDataMap(IDataMap source, String path) {
	    if (path.contains("[")) {
	        String[] split = path.split("\\[");
	        String name = split[0];
	        String index = split[1].replace("]","");
	        IData [] dataArr = source.getAsIDataArray(name);
	        return new IDataMap(dataArr[Integer.parseInt(index)]);
	    } else {
	        return source.getAsIDataMap(path);
	    }
	}
	 
		
	// --- <<IS-END-SHARED>> ---
}


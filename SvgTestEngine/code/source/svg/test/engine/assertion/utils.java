package svg.test.engine.assertion;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import javax.activation.UnsupportedDataTypeException;
import javax.management.RuntimeErrorException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.softwareag.is.metadata.util.IDataMapUtil;
import com.softwareag.util.IDataMap;
import org.apache.commons.lang3.math.NumberUtils;
// --- <<IS-END-IMPORTS>> ---

public final class utils

{
	// ---( internal utility methods )---

	final static utils _instance = new utils();

	static utils _newInstance() { return new utils(); }

	static utils _cast(Object o) { return (utils)o; }

	// ---( server methods )---




	public static final void calculateExecuteTimeUtil (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(calculateExecuteTimeUtil)>> ---
		// @sigtype java 3.5
		// [i] field:0:required startTimeMs
		// [o] field:0:required time
		IDataMap plMap = new IDataMap(pipeline);
		Long startTimeMilis = null;
		Long endTimeMilis = new Date().getTime();
		String startTime = plMap.getAsString("startTimeMs", String.valueOf((endTimeMilis)));
		if (NumberUtils.isParsable(startTime)) {
			startTimeMilis = Long.valueOf(startTime);
		} else {
			startTimeMilis = endTimeMilis;
		}
		plMap.put("time", String.valueOf(endTimeMilis - startTimeMilis));
		// --- <<IS-END>> ---

                
	}



	public static final void contains (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(contains)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] - record:0:required actual
		// [i] -- record:0:required doc
		// [i] --- field:0:required ele
		// [i] field:0:required path
		// [i] field:0:required value
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			if (actualData == null) {
				addAssertionResultToPipeline(pipeline, false, "Expected data type String found null");
				return;
			}
			if (actualData instanceof String) {
				boolean result = ((String) actualData).contains(wrapper.getValue());
				addAssertionResultToPipeline(pipeline,
						result,
						new StringBuilder("Actual data should contain : ")
						.append(wrapper.getValue())
						.append(" but not found in String : ")
						.append(wrapper.getActual()).toString()
						);
			} else {
				addAssertionResultToPipeline(pipeline, false, "Expected data type String but found" + actualData.getClass().getSimpleName());
			}
		} catch (Exception e) {
			addErrorMessageToPipeline(pipeline, e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void doesNotContains (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(doesNotContains)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] field:0:required path
		// [i] field:0:required value
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			if (actualData == null) {
				addAssertionResultToPipeline(pipeline, false, "Expected data type String found null");
				return;
			}
			if (actualData instanceof String) {
				boolean result = ((String) actualData).contains(wrapper.getValue());
				addAssertionResultToPipeline(pipeline,
						!result,
						new StringBuilder("Actual data should not contain : ")
						.append(wrapper.getValue())
						.append(" but value was found in String : ")
						.append(wrapper.getActual()).toString()
						);
			} else {
				addAssertionResultToPipeline(pipeline, false, "Expected data type String but found " + actualData.getClass().getSimpleName());
			}
		} catch (Exception e) {
			addErrorMessageToPipeline(pipeline, e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void doesNotMatchesRegex (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(doesNotMatchesRegex)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] field:0:required path
		// [i] field:0:required value
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			if (actualData == null) {
				addAssertionResultToPipeline(pipeline, false, "Expected data type String found null");
				return;
			}
			if (actualData instanceof String) {
				Pattern pattern = Pattern.compile(wrapper.getValue());
				Matcher matcher = pattern.matcher((String)actualData);
				boolean result = matcher.matches();
				addAssertionResultToPipeline(pipeline,
						!result,
						new StringBuilder("Actual data should not match pattern : ")
						.append(wrapper.getValue())
						.append(" but found match in String : ")
						.append(wrapper.getActual()).toString()
						);
			} else {
				addAssertionResultToPipeline(pipeline, false, "Expected data type String but found" + actualData.getClass().getSimpleName());
			}
		} catch (Exception e) {
			addErrorMessageToPipeline(pipeline, e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getTimestampMilis (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getTimestampMilis)>> ---
		// @sigtype java 3.5
		// [o] field:0:required currentTimeInMilis
		IDataMap map = new IDataMap(pipeline);
		map.put("currentTimeInMilis", String.valueOf(new Date().getTime()));
		// --- <<IS-END>> ---

                
	}



	public static final void isEqual (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isEqual)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] - record:0:required actual
		// [i] -- record:0:required doc
		// [i] --- field:0:required ele
		// [i] field:0:required path
		// [i] field:0:required value
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			String actual = String.valueOf(actualData);
			boolean res = Objects.equals(actual, wrapper.getValue());
			addAssertionResultToPipeline(pipeline, res, "Actual value : " + actual + " is not equal to expected : " + wrapper.getValue());
		} catch (Throwable t) {
			addErrorMessageToPipeline(pipeline, t); 
		}
			
		// --- <<IS-END>> ---

                
	}



	public static final void isGreatherThen (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isGreatherThen)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] field:0:required path
		// [i] field:0:required value
		// [i] record:0:required testData
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			
			if (actualData == null){
				addFailureErrorMessageToPipeline(pipeline, "Expected not null value");
				return;
			}
			BigDecimal bigDec = asBigDecimal(actualData);
			if (bigDec != null) {
				BigDecimal value = bigDec;
				BigDecimal expectedValue = objectToBigDecimal(wrapper.getValue());
				boolean result = value.compareTo(expectedValue) == 1;
				addAssertionResultToPipeline(pipeline, result, "Actual value is " + value + ", expected greather then " + expectedValue);
			} else if (actualData.getClass().isArray()) {
				int expectedLenght = Integer.parseInt(wrapper.getValue());
				Object[] datas = (Object[]) actualData;
				boolean result = datas.length > expectedLenght;
				addAssertionResultToPipeline(pipeline, result, "Actual array size is " + datas.length + ", expected greather then " + expectedLenght);
			} else if (actualData instanceof String) {
				String value = (String) actualData;
				int actualLength = value.length();
				int expectedLenght = Integer.parseInt(wrapper.getValue());
				boolean res = actualLength > expectedLenght;
				addAssertionResultToPipeline(pipeline, res, "Actual string length is " + actualLength + ", expected greather then " + expectedLenght);
			} else {
				addFailureErrorMessageToPipeline(pipeline, actualData.getClass() + " is not supported");
			}
		} catch (Throwable t) {
			addErrorMessageToPipeline(pipeline, t);
		}
			
		// --- <<IS-END>> ---

                
	}



	public static final void isGreatherThenOrEqual (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isGreatherThenOrEqual)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] field:0:required path
		// [i] field:0:required value
		// [i] record:0:required testData
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			
			if (actualData == null){
				addFailureErrorMessageToPipeline(pipeline, "Expected not null value");
				return;
			}
			BigDecimal bigDec = asBigDecimal(actualData);
			if (bigDec != null) {
				BigDecimal value = bigDec;
				BigDecimal expectedValue = objectToBigDecimal(wrapper.getValue());
				boolean result = value.compareTo(expectedValue) != -1;
				addAssertionResultToPipeline(pipeline, result, "Actual value is " + value + ", expected greather then or equal " + expectedValue);
			} else if (actualData.getClass().isArray()) {
				int expectedLenght = Integer.parseInt(wrapper.getValue());
				Object[] datas = (Object[]) actualData;
				boolean result = datas.length >= expectedLenght;
				addAssertionResultToPipeline(pipeline, result, "Actual array size is " + datas.length + ", expected greather then or equal " + expectedLenght);
			} else if (actualData instanceof String) {
				String value = (String) actualData;
				int actualLength = value.length();
				int expectedLenght = Integer.parseInt(wrapper.getValue());
				boolean res = actualLength >= expectedLenght;
				addAssertionResultToPipeline(pipeline, res, "Actual string length is " + actualLength + ", expected greather then or equal " + expectedLenght);
			} else {
				addFailureErrorMessageToPipeline(pipeline, actualData.getClass() + " is not supported");
			}
		} catch (Throwable t) {
			addErrorMessageToPipeline(pipeline, t);
		}
			
		// --- <<IS-END>> ---

                
	}



	public static final void isLessThen (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isLessThen)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] field:0:required path
		// [i] field:0:required value
		// [i] record:0:required testData
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			
			if (actualData == null){
				addFailureErrorMessageToPipeline(pipeline, "Expected not null value");
				return;
			}
			BigDecimal bigDec = asBigDecimal(actualData);
			if (bigDec != null) {
				BigDecimal value = bigDec;
				BigDecimal expectedValue = objectToBigDecimal(wrapper.getValue());
				boolean result = value.compareTo(expectedValue) == -1;
				addAssertionResultToPipeline(pipeline, result, "Actual value is " + value + ", expected lest then " + expectedValue);
			} else if (actualData.getClass().isArray()) {
				int expectedLenght = Integer.parseInt(wrapper.getValue());
				Object[] datas = (Object[]) actualData;
				boolean result = datas.length < expectedLenght;
				addAssertionResultToPipeline(pipeline, result, "Actual array size is " + datas.length + ", expected less then " + expectedLenght);
			} else if (actualData instanceof String) {
				String value = (String) actualData;
				int actualLength = value.length();
				int expectedLenght = Integer.parseInt(wrapper.getValue());
				boolean res = actualLength < expectedLenght;
				addAssertionResultToPipeline(pipeline, res, "Actual string length is " + actualLength + ", expected less then " + expectedLenght);
			} else {
				addFailureErrorMessageToPipeline(pipeline, actualData.getClass() + " is not supported");
			}
		} catch (Throwable t) {
			addErrorMessageToPipeline(pipeline, t);
		}
			
		// --- <<IS-END>> ---

                
	}



	public static final void isLessThenOrEqual (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isLessThenOrEqual)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] field:0:required path
		// [i] field:0:required value
		// [i] record:0:required testData
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			
			if (actualData == null){
				addFailureErrorMessageToPipeline(pipeline, "Expected not null value");
				return;
			}
			BigDecimal bigDec = asBigDecimal(actualData);
			if (bigDec != null) {
				BigDecimal value = bigDec;
				BigDecimal expectedValue = objectToBigDecimal(wrapper.getValue());
				boolean result = value.compareTo(expectedValue) != 1;
				addAssertionResultToPipeline(pipeline, result, "Actual value is " + value + ", expected lest then or equal " + expectedValue);
			} else if (actualData.getClass().isArray()) {
				int expectedLenght = Integer.parseInt(wrapper.getValue());
				Object[] datas = (Object[]) actualData;
				boolean result = datas.length <= expectedLenght;
				addAssertionResultToPipeline(pipeline, result, "Actual array size is " + datas.length + ", expected less then or equal " + expectedLenght);
			} else if (actualData instanceof String) {
				String value = (String) actualData;
				int actualLength = value.length();
				int expectedLenght = Integer.parseInt(wrapper.getValue());
				boolean res = actualLength <= expectedLenght;
				addAssertionResultToPipeline(pipeline, res, "Actual string length is " + actualLength + ", expected less then or equal " + expectedLenght);
			} else {
				addFailureErrorMessageToPipeline(pipeline, actualData.getClass() + " is not supported");
			}
		} catch (Throwable t) {
			addErrorMessageToPipeline(pipeline, t);
		}
			
		// --- <<IS-END>> ---

                
	}



	public static final void isNotEqual (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isNotEqual)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] - record:0:required actual
		// [i] -- record:0:required doc1
		// [i] --- field:0:required ele
		// [i] field:0:required path
		// [i] field:0:required value
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			boolean res = !Objects.equals(actualData, wrapper.getValue());
			addAssertionResultToPipeline(pipeline, res, "Expected obejct is equal to Actual when it shouldn't be.");
		} catch (Throwable t) {
			addErrorMessageToPipeline(pipeline, t); 
		}
			
		// --- <<IS-END>> ---

                
	}



	public static final void isNotNull (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isNotNull)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] - record:0:required actual
		// [i] -- record:0:required doc1
		// [i] --- field:0:required ele
		// [i] field:0:required path
		// [i] field:0:optional value
		// [i] record:0:required testData
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipelineWithoutValue(pipeline);
			Object actualData = wrapper.getActual();
			boolean result = actualData != null;
			addAssertionResultToPipeline(pipeline, result, "Expected non null value");
		} catch (Throwable t) {
			addErrorMessageToPipeline(pipeline,
					new ServiceException("Exception with msg: " + t.getMessage() + " class : " + t.getClass().getCanonicalName())); 
		}
			
		// --- <<IS-END>> ---

                
	}



	public static final void isNull (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(isNull)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] field:0:required path
		// [i] field:0:optional value
		// [i] record:0:required testData
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipelineWithoutValue(pipeline);
			Object actualData = wrapper.getActual();
			boolean result = actualData == null;
			addAssertionResultToPipeline(pipeline, result, "Expected null but was: " + actualData);
		} catch (Throwable t) {
			addErrorMessageToPipeline(pipeline, t); 
		}
			
		// --- <<IS-END>> ---

                
	}



	public static final void matchesRegex (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(matchesRegex)>> ---
		// @sigtype java 3.5
		// [i] record:0:required $pipeline
		// [i] - record:0:required actual
		// [i] -- record:0:required doc1
		// [i] --- field:0:required ele
		// [i] field:0:required path
		// [i] field:0:required value
		// [o] field:0:required error
		// [o] field:0:required failure
		// [o] field:0:required success
		try {
			DataWrapper wrapper = DataWrapper.fromPipeline(pipeline);
			Object actualData = wrapper.getActual();
			if (actualData == null) {
				addAssertionResultToPipeline(pipeline, false, "Expected data type String found null");
				return;
			}
			if (actualData instanceof String) {
				Pattern pattern = Pattern.compile(wrapper.getValue());
				Matcher matcher = pattern.matcher((String)actualData);
				boolean result = matcher.matches();
				addAssertionResultToPipeline(pipeline,
						result,
						new StringBuilder("Actual data should match pattern : ")
						.append(wrapper.getValue())
						.append(" but didn't found match in String : ")
						.append(wrapper.getActual()).toString()
						);
			} else {
				addAssertionResultToPipeline(pipeline, false, "Expected data type String but found" + actualData.getClass().getSimpleName());
			}
		} catch (Exception e) {
			addErrorMessageToPipeline(pipeline, e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void pipelineEqualValidator (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(pipelineEqualValidator)>> ---
		// @sigtype java 3.5
		// [i] field:0:required actualJSON
		// [i] field:0:required expectedJSON
		// [i] object:0:required failOnFirstAssertion
		// [o] field:1:required errors
		// [o] field:1:required failures
		// [o] field:0:required success
		IDataMap pipelineMap = new IDataMap(pipeline);
		List<String> failures = new ArrayList<String>();
		boolean noError = true;
		try {
			String actualJSON = pipelineMap.getAsNonEmptyString("actualJSON");
			String expectedJSON = pipelineMap.getAsNonEmptyString("expectedJSON");
			Boolean failOnFirstAssertion = pipelineMap.getAsBoolean("failOnFirstAssertion", true);
			if (actualJSON.equals(expectedJSON)) {
				// JSON object representation is equal then assertion is completed
				return;
			}
			Map<String,String> expectedMap = convert(expectedJSON);
			Map<String, String> actualMap = convert(actualJSON);
			if (expectedMap == null || actualMap == null) {
				failures.add("Excpected value " + expectedJSON + " did not match atual " + actualJSON );
		        return;
			}
			if (expectedMap.equals(actualMap)) {
				// object hash map representation is equal then assertion is completed
				return;
			}
			Set<String> expetedKeySet = new HashSet<>(expectedMap.keySet());
		    Set<String> actualKeySet = new HashSet<>(actualMap.keySet());
		    expetedKeySet.removeAll(actualKeySet);
		    if (expetedKeySet.size() > 0) {
		        // missing required data (key) in actualMap 
		        for (String element : expetedKeySet) {
		        	failures.add(
		        			new StringBuilder("Expected element ")
		        			.append(element)
		        			.append(" not found in actual data")
		        			.toString()
		        			);
		        }
		        return;
		    }
		    expetedKeySet = new HashSet<>(expectedMap.keySet());
		    actualKeySet.removeAll(expetedKeySet);
		    if (actualKeySet.size() > 0) {
		        // actual object contains unexpected data
		    	for (String element : actualKeySet) {
		    		failures.add(
		    				new StringBuilder("Unexpected element ")
		    				.append(element)
		    				.append(" found in actual data")
		    				.toString()
		    				);
		    	}
		        return;
		    }
		    for (String key : expetedKeySet) {
		        String actualValue = actualMap.get(key);
		        String expectedValue = expectedMap.get(key);
		        if (!Objects.equals(actualValue, expectedValue)) {
		            // expected value does not match actual value
		        	failures.add(
		        			new StringBuilder("Assertion error on element ")
		        			.append(key)
		        			.append("expected : ")
		        			.append(expectedValue)
		        			.append(" found : ")
		        			.append(actualValue)
		        			.toString()
		        			);
		            if (failOnFirstAssertion) {
		            	break;
		            }
		        }
		    }
		} catch (Throwable e) {
			noError = false;
			addErrorMessagesToPipeline(pipeline, e);
		} finally {
			addAssertionResultesToPipeline(pipeline, failures.isEmpty() && noError, failures);
		}
		
			
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	
	private static BigDecimal asBigDecimal(Object data) throws UnsupportedDataTypeException {
		if (data instanceof String) {
			String input = ((String)data).replaceAll(",", ".");
			if (NumberUtils.isParsable(input)) {
				return NumberUtils.createBigDecimal(input);
			} else {
				return null;
			}
		} else if (data instanceof Number) {
			return objectToBigDecimal(data);
		} else {
			return null;
		}
		
	}
	
	private static void addFailureErrorMessageToPipeline(IData pipeline, String errorMessage) {
		addAssertionResultToPipeline(pipeline, false, errorMessage);
	}
	
	private static void addAssertionResultToPipeline(IData pipeline, boolean isOK, String failureMessage) {
		IDataMap datamap = new IDataMap(pipeline);
		if (isOK) {
			datamap.put("success", String.valueOf(true));
		} else {
			datamap.put("failure", failureMessage != null && failureMessage.length() > 0 ? failureMessage : "Assertion error");
			datamap.put("success", String.valueOf(false));
		}
	}
	
	private static void addErrorMessageToPipeline(IData pipeline, Throwable exception) {
		IDataMap datamap = new IDataMap(pipeline);
		datamap.put("error", exception.getMessage() != null ? exception.getMessage() : exception.toString());
		datamap.put("success", String.valueOf(false));
	}
	
	private static void addAssertionResultesToPipeline(IData pipeline, boolean isOK, List<String> failures) {
		IDataMap datamap = new IDataMap(pipeline);
		if (isOK) {
			datamap.put("success", String.valueOf(true));
		} else {
			datamap.put("failures", failures.toArray(new String[failures.size()]));
			datamap.put("success", String.valueOf(false));
		}
	}
	
	private static void addErrorMessagesToPipeline(IData pipeline, Throwable exception) {
		IDataMap datamap = new IDataMap(pipeline);
		List<String> errList = Collections.singletonList(exception.getMessage());
		datamap.put("errors", errList.toArray(new String[errList.size()]));
		datamap.put("success", String.valueOf(false));
	}
	
	static class DataWrapper {
		private String path;
		private String value;
		private IDataMap actualPipelineMap;
		private Object actual;
		
		public static DataWrapper fromPipelineWithoutValue(IData pipeline) throws NoSuchFieldException {
			return fromPipeline(pipeline , false);
		}
		
		public static DataWrapper fromPipeline(IData pipeline) throws NoSuchFieldException {
			return fromPipeline(pipeline , true);
		}
		
		public static DataWrapper fromPipeline(IData pipeline, boolean withValue) throws NoSuchFieldException {
			IDataMap map = new IDataMap(pipeline);
			IDataMap actualPipelineMap = map.getAsIDataMap("$pipeline");
			actualPipelineMap = actualPipelineMap.getAsIDataMap("actual");
			String path = map.getAsNonEmptyString("path"); 
			String value = null;
			if (withValue) {
				value = map.getAsNonEmptyString("value");
			}
			Object actualData = getElementForAssertion(actualPipelineMap, path);
			DataWrapper dw = new DataWrapper(path,value, actualPipelineMap, actualData);
			return dw;
		}
		
		private DataWrapper(String path, String value, IDataMap actualPipelineMap, Object actual) {
			super();
			this.path = path;
			this.value = value;
			this.actualPipelineMap = actualPipelineMap;
			this.actual = actual;
		}
		
		public String getPath() {
			return path;
		}
		public String getValue() {
			return value;
		}
		public IDataMap getActualPipelineMap() {
			return actualPipelineMap;
		}
		public Object getActual() {
			return actual;
		}
		
		private static Object getElementForAssertion(IDataMap actualPipelineMap, String path) throws NoSuchFieldException {
			Object actualData = null;
			if (!path.contains("/")) {
				actualData = getAssertedElement(actualPipelineMap, path);
			} else {
				List<String> pathElements = Arrays.asList(path.split("/"));
				String root = pathElements.get(0);
				IDataMap dataMap = getAsIDataMap(actualPipelineMap, root);
				String fieldName = pathElements.get(pathElements.size()-1 );
				for (int i=1 ; i < pathElements.size() -1; i++) {
					dataMap = getAsIDataMap(dataMap, pathElements.get(i));
				}
				String assertedElementName = pathElements.get(pathElements.size() - 1);
				actualData = getAssertedElement(dataMap, assertedElementName);
			}
			return actualData;
		}
		
		private static Object getAssertedElement(IDataMap source, String path) throws NoSuchFieldException {
			if (!path.contains("[")) {
				return source.get(path);
			} else {
				String[] split = path.split("\\[");
			    String name = split[0];
			    Object o = source.get(name);
			    if (o.getClass().isArray()) {
			    	Integer index = Integer.valueOf(split[1].replace("]",""));
			    	return ((Object[])o)[index];
			    }
			    throw new NoSuchFieldException("Unable to find path element " + path);
			}
		}
		
		private static IDataMap getAsIDataMap(IDataMap source, String path) {
		    if (path.contains("[")) {
		        String[] split = path.split("\\[");
		        String name = split[0];
		        String index = split[1].replace("]","");
		        IData [] dataArr = source.getAsIDataArray(name);
		        return new IDataMap(dataArr[Integer.parseInt(index)]);
		    } else {
		        return new IDataMap(source.getAsIDataMap(path));
		    }
		}
		
	}
	
	private static BigDecimal objectToBigDecimal(Object value) throws UnsupportedDataTypeException {
	    if (value instanceof BigDecimal) {
	        return (BigDecimal)value;
	    }
	    
	    if (value instanceof String) {
	        return new BigDecimal((String)value);
	    } else if (value instanceof Integer) {
	    	return new BigDecimal((Integer)value);
	    } else if (value instanceof Long) {
	    	return new BigDecimal((Long)value);
	    } else if (value instanceof Double) {
	    	Double val = (Double) value;
	//	    	if(1==1) throw new RuntimeException("value is " + val);
			int length = val.toString().length();
			final int i = val.toString().replaceAll(",", ".").indexOf(".");
			int scale = length - i - 1;
			return new BigDecimal(val).setScale(scale, RoundingMode.HALF_UP);
	    } else if (value instanceof Float) {
			Float floatValue = (Float) value;
			int length = floatValue.toString().length();
			final int i = floatValue.toString().replaceAll(",", ".").indexOf(".");
			int scale = length - i - 1;
			return new BigDecimal(floatValue).setScale(scale, RoundingMode.HALF_UP);
	    } else if (value instanceof Short) {
	    	return new BigDecimal((Short)value);
	    }  
	    throw new UnsupportedDataTypeException("Invalid number data type."
	    		+ "Supperted data types are [Integer, Long, Float, Double, Short, BigDecimal] but found : "
	    		+ value.getClass().getSimpleName());
	}
	
	public static Map<String, String> convert(String json) throws Exception {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String, Object>>() {
			};
			JsonNode jsonNode = objectMapper.readTree(json);
			Map<String, Object> input = objectMapper.convertValue(jsonNode, typeReference);
			Map<String, String> result = new HashMap<String, String>();
			for (Map.Entry<String, Object> stringObjectEntry : input.entrySet()) {
				final Object value = stringObjectEntry.getValue();
				final Map<String, String> valueWithKeyPath = getValueWithKeyPath(stringObjectEntry.getKey(), (LinkedHashMap<String, Object>) value);
				result.putAll(valueWithKeyPath);
			}
			return result;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Map<String, String> getValueWithKeyPath(String prefix, LinkedHashMap<String, Object> value) {
	    Map<String, String> result = new HashMap<String, String>();
	    for (Map.Entry<String, Object> entry : value.entrySet()) {
	        final String p = prefix + "/" + entry.getKey();
	        if (entry.getValue() instanceof String) {
	            result.put(p, (String) entry.getValue());
	        } else if (entry.getValue() instanceof ArrayList) {
	            final List<LinkedHashMap<String, Object>> array = (List<LinkedHashMap<String, Object>>) entry.getValue();
	            for (int i = 0; i < array.size(); i++) {
	                String pArray = p + "[" + i + "]";
	                final Map<String, String> tmpResult = getValueWithKeyPath(pArray, array.get(i));
	                result.putAll(tmpResult);
	            }
	        } else if (entry.getValue() instanceof LinkedHashMap) {
	            final Map<String, String> tmpResult = getValueWithKeyPath(p, (LinkedHashMap<String, Object>) entry.getValue());
	            result.putAll(tmpResult);
	        }
	    }
	    return result;
	}	
	// --- <<IS-END-SHARED>> ---
}


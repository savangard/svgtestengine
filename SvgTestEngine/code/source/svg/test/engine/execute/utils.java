package svg.test.engine.execute;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.ServiceThread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.softwareag.is.health.indicators.ServiceThreadHealthIndicator;
import com.softwareag.util.IDataMap;
import com.wm.lang.ns.NSName;
// --- <<IS-END-IMPORTS>> ---

public final class utils

{
	// ---( internal utility methods )---

	final static utils _instance = new utils();

	static utils _newInstance() { return new utils(); }

	static utils _cast(Object o) { return (utils)o; }

	// ---( server methods )---




	public static final void deleteDuplicatesFromStringList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(deleteDuplicatesFromStringList)>> ---
		// @sigtype java 3.5
		// [i] field:1:required inputList
		// [o] field:1:required outputList
		IDataCursor inCursor = pipeline.getCursor();
		String[] inputList = IDataUtil.getStringArray(inCursor, "inputList");
		inCursor.destroy();
		
		Set<String> inputSet = Stream.of(inputList).collect(Collectors.toSet());
		String[] outputList = inputSet.toArray(new String[inputSet.size()]);
			
		IDataCursor outputCursor = pipeline.getCursor();
		IDataUtil.put(outputCursor, "outputList", outputList);
		outputCursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void deleteEmptyElementsFromStringList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(deleteEmptyElementsFromStringList)>> ---
		// @sigtype java 3.5
		// [i] field:1:required inputList
		// [o] field:1:required outputList
		IDataCursor inCursor = pipeline.getCursor();
		String[] inputList = IDataUtil.getStringArray(inCursor, "inputList");
		inCursor.destroy();
		
		if(inputList != null) {
			List<String> stringList = Stream.of(inputList).collect(Collectors.toList());
			stringList.removeAll(Collections.singletonList(""));
		
			if (stringList.size() > 0) {
				String[] outputList = stringList.toArray(new String[stringList.size()]);
				IDataCursor outputCursor = pipeline.getCursor();
				IDataUtil.put(outputCursor, "outputList", outputList);
				outputCursor.destroy();
			}
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getMatchingStrings (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getMatchingStrings)>> ---
		// @sigtype java 3.5
		// [i] field:1:required inputList
		// [i] field:0:required regex
		// [o] field:1:optional outputList
		IDataCursor inCursor = pipeline.getCursor();
		String[] inputList = IDataUtil.getStringArray(inCursor, "inputList");
		String regex = (String) IDataUtil.getString(inCursor, "regex");
		inCursor.destroy();  
		
		Set<String> inputSet = Stream.of(inputList).collect(Collectors.toSet());
		ArrayList<String> matches = new ArrayList<String>();
		
		for (String s:inputSet) {
			if (Pattern.matches(regex, s)) {
				matches.add(s);
		    }
		}
		
		if(!matches.isEmpty()) {
			String[] outputList = matches.toArray(new String[matches.size()]);
			IDataCursor outputCursor = pipeline.getCursor();
			IDataUtil.put(outputCursor, "outputList", outputList);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void getTestCaseContext (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getTestCaseContext)>> ---
		// @sigtype java 3.5
		// [o] field:0:optional testCaseContext
		String testCaseContext = (String) svg.test.engine.mock.context.currentTestCase.get();
		
		IDataCursor cursor = pipeline.getCursor();
		IDataUtil.put(cursor, "testCaseContext", testCaseContext);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void getTestSuiteContext (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getTestSuiteContext)>> ---
		// @sigtype java 3.5
		// [o] field:0:optional testSuiteContext
		String testSuiteContext = (String) svg.test.engine.mock.context.currentTestSuite.get();
		
		IDataCursor cursor = pipeline.getCursor();
		IDataUtil.put(cursor, "testSuiteContext", testSuiteContext);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void invokeService (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(invokeService)>> ---
		// @sigtype java 3.5
		// [i] field:0:required service
		// [i] record:0:optional pipeline
		// [o] record:0:optional pipeline
		IDataCursor cursor = pipeline.getCursor();
		String serviceName = IDataUtil.getString(cursor, "service");
		IData inputPipeline = IDataUtil.getIData(cursor, "pipeline");
		
		if (inputPipeline == null) {
			inputPipeline = IDataFactory.create();
			IDataUtil.put(cursor, "pipeline", inputPipeline);
		}
		
		cursor.destroy();
		
		try {
			Service.doInvoke(NSName.create(serviceName), inputPipeline);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void parallelInvoke (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(parallelInvoke)>> ---
		// @sigtype java 3.5
		// [i] field:0:optional numberOfThreads
		// [i] record:1:optional requests
		// [i] - field:0:required serviceName
		// [i] - record:0:required serviceInputData
		// [o] record:1:optional responses
		// [o] - field:0:required serviceName
		// [o] - record:0:required serviceOutputData
		// [o] - field:0:required exceptionMessage
		IDataCursor inCursor = pipeline.getCursor();
		IData[] requestsData = IDataUtil.getIDataArray(inCursor, "requests");
		
		if (requestsData != null) {
			int numberOfThreads 
				= ((IDataUtil.getString(inCursor, "numberOfThreads") == null) ? requestsData.length 
						: Integer.parseInt(IDataUtil.getString(inCursor, "numberOfThreads")));
			inCursor.destroy();
			
		    IData[] responsesData = new IData[requestsData.length];
		    String[] servicesName = new String[requestsData.length];
		    IData[] servicesInputData = new IData[requestsData.length];
		    IData[] servicesOutputData = new IData[requestsData.length];
		    String[] exceptionsMessage = new String[requestsData.length];
		    for (int i = 0; i < requestsData.length; i++) {
		        IDataCursor cursor = requestsData[i].getCursor();
		        servicesName[i] = IDataUtil.getString(cursor, "serviceName");
		        servicesInputData[i] = IDataUtil.getIData(cursor, "serviceInputData");
		        cursor.destroy();
		    }
		    
		    if (requestsData.length == 1) {
		        try {
		            servicesOutputData[0] = Service.doInvoke(NSName.create(servicesName[0]), servicesInputData[0]);
		        }
		        catch (Exception e) {
		            exceptionsMessage[0] = e.toString();
		        }	        
		    }
		    else {
		        ServiceThread[] threads = new ServiceThread[numberOfThreads];
		        for (int n = 0; n < requestsData.length; n++) {
		            try {
		            	if(numberOfThreads == 1) {
		            		threads[0] = Service.doThreadInvoke(NSName.create(servicesName[n]), servicesInputData[n]);
		            		servicesOutputData[n] = threads[0].getData();
		            		
		            	} else {
		            		threads[n] = Service.doThreadInvoke(NSName.create(servicesName[n]), servicesInputData[n]);
		            	}
		            }
		            catch (Exception e) {
		                throw new ServiceException(e);
		            }		            
		        }
		        
		        for (int n = 0; n < requestsData.length; n++) {
		            try {
		            	if(numberOfThreads == 1) {
		            		//servicesOutputData[n] = threads[0].getData();
		            	} else {
		            		servicesOutputData[n] = threads[n].getData();
		            	}
		            }
		            catch (Exception e) {
		                exceptionsMessage[n] = e.toString();
		            }		            
		        }		        
		    }
		    
		    for (int n = 0; n < requestsData.length; n++) {
		        responsesData[n] = IDataFactory.create();
		        IDataCursor cursor = responsesData[n].getCursor();
		        IDataUtil.put(cursor, "serviceName", servicesName[n]);
		        IDataUtil.put(cursor, "serviceOutputData", servicesOutputData[n]);
		        if (exceptionsMessage[n] != null) {
		            IDataUtil.put(cursor, "exceptionMessage", exceptionsMessage[n]);
		        }		        
		        cursor.destroy();
		    }
		    
		    IDataCursor outCursor = pipeline.getCursor();
		    IDataUtil.put(outCursor, "responses", responsesData);
		    outCursor.destroy();
		}
		// --- <<IS-END>> ---

                
	}



	public static final void pipelineToDocument (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(pipelineToDocument)>> ---
		// @sigtype java 3.5
		// [o] record:0:required pipelineDocument
		try {
			IDataCursor cursor = pipeline.getCursor();
			IDataMap map = new IDataMap(pipeline);
			IData[] assertions = map.getAsIDataArray("assertions");
			String testCaseName = map.getAsString("caseName");
			Boolean failOnFirstAssertion = new Boolean(map.getAsBoolean("failOnFirstAssertion", true));
			IData header = map.getAsIData("header");
			IData headerAssertion = map.getAsIData("headerAssertion");
			
			IData[] putAssertions = null;
			if (assertions != null && assertions.length > 0) {
				putAssertions = new IData[assertions.length];
				for (int i = 0; i < assertions.length; i++) {
					putAssertions[i] = IDataUtil.deepClone(assertions[i]);
				}
			}
			String putTestCaseName = null;
			if (testCaseName != null) {
				putTestCaseName = new String(testCaseName);
			}
			
			IData putHeader = null;
			IData putHeaderAssertion = null;
			if (header != null) {
				putHeader = IDataUtil.deepClone(header);
			}
			if (headerAssertion != null) {
				putHeaderAssertion = IDataUtil.deepClone(headerAssertion);
			}
			
			IData doc = IDataFactory.create();
			IDataCursor idc = doc.getCursor();	
			
			while(cursor.next()) {
				idc.insertAfter(cursor.getKey(), cursor.getValue());		
			}
			
			idc.destroy();
			cursor.first();
			while(cursor.delete());
			
			cursor.insertBefore("pipelineDocument", doc);
			if (putTestCaseName != null) {
				cursor.insertAfter("caseName", putTestCaseName);
			} 
			if (putAssertions != null) {
				cursor.insertAfter("assertions", putAssertions);
			}
			if (putHeader != null) {
				cursor.insertAfter("header",putHeader);
			}
			if (putHeaderAssertion != null) {
				cursor.insertAfter("headerAssertion",putHeaderAssertion);
			}
			cursor.insertAfter("failOnFirstAssertion", failOnFirstAssertion);
			cursor.destroy();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void sleep (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(sleep)>> ---
		// @sigtype java 3.5
		// [i] field:0:required millis
		IDataCursor pipelineCursor = pipeline.getCursor();
		try {
			long millis = Long.parseLong(IDataUtil.getString(pipelineCursor, "millis"));
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		} finally {
			pipelineCursor.destroy();
		}
		// --- <<IS-END>> ---

                
	}
}


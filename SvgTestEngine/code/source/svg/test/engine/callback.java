package svg.test.engine;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.lang.flow.FlowInvoke;
import com.wm.lang.flow.FlowMap;
import com.wm.lang.flow.FlowMapCopy;
import com.wm.lang.flow.FlowMapDelete;
import com.wm.lang.flow.FlowMapSet;
import com.wm.lang.flow.FlowRoot;
import com.wm.lang.ns.NSField;
import com.wm.lang.ns.NSName;
import com.wm.lang.ns.NSRecord;
import com.wm.lang.ns.NSService;
import com.wm.lang.ns.NSSignature;
import com.wm.util.JavaWrapperType;
import com.wm.util.UUID;
import java.util.ArrayList;
import java.util.HashMap;
import com.softwareag.util.IDataMap;
import com.wm.app.b2b.server.Package;
import com.wm.app.b2b.server.PackageManager;
import com.wm.app.b2b.server.ServerAPI;
import com.wm.app.b2b.server.ns.Interface;
import com.wm.app.b2b.server.ns.Namespace;
// --- <<IS-END-IMPORTS>> ---

public final class callback

{
	// ---( internal utility methods )---

	final static callback _instance = new callback();

	static callback _newInstance() { return new callback(); }

	static callback _cast(Object o) { return (callback)o; }

	// ---( server methods )---




	public static final void createCallbackService (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(createCallbackService)>> ---
		// @sigtype java 3.5
		// [i] field:0:required package
		// [i] field:0:required folder
		// [i] field:0:optional uuid
		// [o] field:0:required uuid
		// [o] field:0:required serviceName
		IDataMap output = new IDataMap(pipeline);
		
		String folder         = output.getAsString("folder");
		String packageName    = output.getAsString("package"); 
		String uuid           = output.getAsString("uuid", UUID.generate());		
		
		Namespace ns = Namespace.current();
		Package pkg = PackageManager.getPackage(packageName);
		if (pkg == null) {
			throw new ServiceException("Cannot create notifier service: package '"+ packageName + "' does not exist");
		}
		try {
			//First create 'folder' in 'package' if it doesn't exist
			Interface ifc = new Interface(NSName.create(folder));
			
			if (ifc == null) {
				throw new ServiceException("Illegal folder name: '"+ folder +"'");
			}
			
			if (!pkg.getStore().descriptionPathExists(ifc.getNSName())) {
				ns.putNode(ifc, new Package(packageName));
			}
						
			uuid = uuid.replace("-", "");			
						
			NSRecord inSig = new NSRecord(ns, "in", NSField.DIM_SCALAR);
			NSRecord dataRecord = new NSRecord(ns, "data", NSField.DIM_SCALAR );
			inSig.addField(dataRecord);
			
			NSRecord outSig = new NSRecord(ns, "out", NSField.DIM_SCALAR);
			NSField notifiedField = new NSField(ns, "notified", NSField.FIELD_OBJECT, NSField.DIM_SCALAR);
			notifiedField.setJavaWrapperType(JavaWrapperType.JAVA_TYPE_INTEGER);
			outSig.addField(notifiedField);
						
			NSSignature sig = new NSSignature(inSig, outSig);
			
			//Create flow service
			Object [][] rootValues = {{"type", "ROOT"}};
			FlowRoot flow = new FlowRoot(new Values(rootValues));
			
			Values invokeValues = new Values();
			invokeValues.put("type", "INVOKE");
			invokeValues.put("comment", "Notify with generated key "+ uuid);
			invokeValues.put("service", "pub.sync:notify");
			
			IDataMap invokeData = new IDataMap();
			invokeData.put("type", "INVOKE");
			invokeData.put("comment", "Notifty with generated key '" + uuid + "'");
			invokeData.put("service", "pub.sync:notify");
							
			FlowInvoke invoke = new FlowInvoke(invokeData.getIData());
			
			flow.addNode(invoke);
			
			FlowMap notifyMap = new FlowMap(null);
			FlowMapCopy valueCopy = new FlowMapCopy(null);
			valueCopy.setMapFrom("data", NSField.FIELD_RECORD, NSField.DIM_SCALAR);
			valueCopy.setMapTo("value", NSField.FIELD_RECORD, NSField.DIM_SCALAR);
									
			FlowMapSet keySet = new FlowMapSet(null);
			keySet.setField("key", NSField.FIELD_STRING, NSField.DIM_SCALAR);
			keySet.setInput(uuid);
			keySet.setOverwrite(true);
						
			notifyMap.addNode(valueCopy);
			notifyMap.addNode(keySet);
		
			invoke.setInputMap(notifyMap);
		
			FlowMap outputMap = new FlowMap(null);
			FlowMapDelete dropKey = new FlowMapDelete(null);
			dropKey.setField("key", NSField.FIELD_STRING, NSField.DIM_SCALAR);
			FlowMapDelete dropValue = new FlowMapDelete(null);
			dropValue.setField("value", NSField.FIELD_RECORD, NSField.DIM_SCALAR);
			outputMap.addNode(dropKey);
			outputMap.addNode(dropValue);
			invoke.setOutputMap(outputMap);
		
			ServerAPI.registerFlowService(packageName, NSName.create(folder, uuid), flow, sig);
		
			output.put("uuid", uuid);
			output.put("serviceName", folder + ":" + uuid);
			
		} catch (Exception e) {
			throw new ServiceException(e.getClass().getName() + ": " + e.getMessage());
		}
		// --- <<IS-END>> ---

                
	}
}


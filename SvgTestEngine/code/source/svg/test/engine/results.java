package svg.test.engine;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import java.util.*;
import java.util.regex.Pattern;
// --- <<IS-END-IMPORTS>> ---

public final class results

{
	// ---( internal utility methods )---

	final static results _instance = new results();

	static results _newInstance() { return new results(); }

	static results _cast(Object o) { return (results)o; }

	// ---( server methods )---




	public static final void getReportsList (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(getReportsList)>> ---
		// @sigtype java 3.5
		// [o] field:1:required reportsList
		IDataCursor pipelineCursor = pipeline.getCursor();
		String[] unnessesaryFolder = {"js", "resources", "reportPage"};
		final File folder = new File("./packages/SvgTestEngine/pub");
		ArrayList<String> reportsList = new ArrayList<String>();
		boolean contain = false;
		
		for (final File f : folder.listFiles()) {
			contain = false;
		    if (f.isDirectory()) {
		    	String fileName = f.getName();
		    	for(int i =0; i < unnessesaryFolder.length; i++)
		    		if(unnessesaryFolder[i].equals(fileName))
		    			contain = true;	    	
		    	if(!contain)
		    		reportsList.add(fileName);
		    }
		}
		
		Collections.sort(reportsList);
		Collections.reverse(reportsList);
		String[] reportsArray = reportsList.toArray(new String[reportsList.size()]);	
		
		IDataUtil.put(pipelineCursor, "reportsList", reportsArray); 
		pipelineCursor.destroy();	
		// --- <<IS-END>> ---

                
	}



	public static final void prepareFileData (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(prepareFileData)>> ---
		// @sigtype java 3.5
		// [i] field:0:required fileData
		// [o] field:1:required values
		IDataCursor pipelineCursor = pipeline.getCursor();
		String fileData = IDataUtil.getString( pipelineCursor, "fileData" );
		boolean isValue = false;
		ArrayList<String> tmpList = new ArrayList<String>();
		String tmp = "";
		int index = 0;
		
		if (fileData != null) {
			for(int i =0; i < fileData.length(); i++){
				char c = fileData.charAt(i);
				if(c == ';')
				{
					isValue = false;
					tmpList.add(tmp);
					index++;
					tmp = "";
				}
				else if(c == '=')
					isValue = true;
				else if(isValue)
						tmp+=c;
			}	
		}
		
		String[] values = new String[index];
		for(int i =0; i < index; i++)
			values[i] = tmpList.get(i);
		
		IDataUtil.put(pipelineCursor, "values",values);
		pipelineCursor.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	
	private static boolean cleanDocument(IData pipeline) {
		boolean removeDocument = false;
		if (pipeline != null) {
			boolean removeTag = false;
			Hashtable<String, Object[]> arrays = new Hashtable<String, Object[]>();
			IDataCursor pipelineCursor = pipeline.getCursor();
			while ((removeTag == false && pipelineCursor.next()) || (removeTag == true && pipelineCursor.delete())) {
				removeTag = false;
				String k = pipelineCursor.getKey();
				Object o = pipelineCursor.getValue();
				if (o == null) {
					removeTag = true;
				} else if (o.getClass().isArray()) {
					Object[] array = cleanArray((Object[]) o);
					if (array == null) {
						removeTag = true;
					} else {
						arrays.put(k, array);
						removeTag = false;
					}
				} else if (o instanceof IData) {
					removeTag = cleanDocument((IData) o);
				} else {
					removeTag = isEmpty(o);
				}
			}
			Enumeration<String> e = arrays.keys();
			while (e.hasMoreElements()) {
				String k = (String) e.nextElement();
				Object[] v = (Object[]) arrays.get(k);
				if (v[0] instanceof IData) {
					IData[] vIData = new IData[v.length];
					System.arraycopy(v, 0, vIData, 0, v.length);
					IDataUtil.put(pipelineCursor, k, vIData);
				}
			}
			removeDocument = (IDataUtil.size(pipelineCursor) == 0) ? true : false;
			pipelineCursor.destroy();
		}
		return removeDocument;
	}
	
	private static boolean isEmpty(Object object) {
		return (object == null || "".equals(object.toString()));
	}
	
	private static Object[] cleanArray(Object[] objects) {
		if (objects == null || objects.length == 0) {
			return null;
		}
		List<Object> destinationRows = new ArrayList<Object>();
		for (Object o : objects) {
			boolean removeRow = false;
			if (o instanceof IData) {
				removeRow = cleanDocument((IData) o);
			} else {
				removeRow = isEmpty(o);
			}
			if (!removeRow) {
				destinationRows.add(o);
			}
		}
		return (destinationRows.size() == 0) ? null : destinationRows.toArray();
	}
	
	private static Pattern RE_TRUE = Pattern.compile("yes|true|1|Y", Pattern.CASE_INSENSITIVE);
	
	private static IData clean(IData idata){
	    IDataCursor cursor = idata.getCursor();
	    boolean elemToDelete = false;
	
	    while ((elemToDelete == false && cursor.next()) || (elemToDelete == true && cursor.delete())) {
	        elemToDelete = false;
	        Object obj = cursor.getValue();
	
	        if (obj == null){
	            elemToDelete = true;
	        }
	        else if (obj instanceof String){
	            if (((String)obj).equals(""))
	                elemToDelete = true;
	        }
	        else if (obj instanceof String[]){
	            String[] items = (String[]) obj;
	            ArrayList<String> newList = new ArrayList<String>();
	
	            for (int i=0; i<items.length ; i++){
	                if (items[i] != null && !"".equals(items[i]) )
	                    newList.add(items[i]);               
	            }               
	            cursor.setValue(newList.toArray(new String[0]));
	        }
	        else if (obj instanceof IData){
	            clean((IData) obj);
	        }           
	        else if (obj instanceof IData[]){
	            IData[] items = (IData[]) obj;
	            ArrayList<IData> newItems = new ArrayList<IData>();
	            for (int i=0; i<items.length ; i++){
	                items[i] = clean(items[i]);
	
	                //check for not empty IData document in array                
	                if (IDataUtil.size(((IData) items[i]).getCursor()) >0 )
	                	
	                	
	                    newItems.add(items[i]);
	            }
	            //set new array without empty documents
	            cursor.setValue(newItems.toArray(new IData[0]));
	        }
	    }
	    cursor.destroy();
	    return idata;
	}
	
	private static IData dropFields(IData idata, List<String> fieldNames){
	    IDataCursor cursor = idata.getCursor();
	    boolean elemToDelete = false;
	
	    while ((elemToDelete == false && cursor.next()) || (elemToDelete == true && cursor.delete())) {
	        elemToDelete = false;
	        String key = cursor.getKey();
	        Object obj = cursor.getValue();
	
	        if (fieldNames.contains(key)){
	            elemToDelete = true;
	        } else if (obj instanceof IData){
	        	dropFields((IData) obj, fieldNames);
	        } else if (obj instanceof IData[]){
	            IData[] items = (IData[]) obj;
	            List<IData> newItemList = new ArrayList<IData>();
	            for (int i=0; i<items.length ; i++){
	                items[i] = dropFields(items[i], fieldNames);
	
	                //check for not empty IData document in array                
	                if (IDataUtil.size(items[i].getCursor()) >0 )
	                	newItemList.add(items[i]);
	            }
	            //set new array without empty documents
	            cursor.setValue(newItemList.toArray(new IData[0]));
	        }
	    }
	    cursor.destroy();
	    return idata;
	}
	
	
	private static boolean cleanupNullIData1(IData data) {
			boolean dataToDelete = false;
			if (data != null) {
				IDataCursor cur = data.getCursor();
				boolean elemToDelete = false;
				Hashtable arrays = new Hashtable();
				while((elemToDelete == false && cur.next()) || (elemToDelete == true && cur.delete())) {
					elemToDelete = false;
					String key = cur.getKey();
					Object obj = cur.getValue();
					if(obj == null)
					elemToDelete = true;
					else if(obj.getClass().isArray()) {
						Object[] array = (Object[])obj;
						Object[] cleanedArray = cleanupNullArray1(array);
						if(cleanedArray == null)
						elemToDelete = true;
						else {
							arrays.put(key, cleanedArray);
							elemToDelete = false;
						}
					} else if(obj instanceof IData) {
						elemToDelete = cleanupNullIData1((IData)obj);
					} else {
						elemToDelete = cleanupNonIData(obj);
					}
				}
				Enumeration enume = arrays.keys();
				while(enume.hasMoreElements()) {
					String key = (String)enume.nextElement();
					Object[] value = (Object[])arrays.get(key);
					if(value[0] instanceof IData) {
						IData[] valueIData = new IData[value.length];
						for(int i = 0; i < valueIData.length; i++) {
							valueIData[i] = (IData)value[i];
						}
						IDataUtil.put(cur, key, valueIData);
					}
				}
				if(IDataUtil.size(cur) == 0)
				dataToDelete = true;
				cur.destroy();
			}
			return dataToDelete;
		}
		
		private static Object[] cleanupNullArray1(Object[] array) {
			Object[] cleanedArray = array;
			if(array == null || array.length == 0)
			return null;
			/*  else if(array[0] == null || array[0].toString().equals(""))  */
			
			
			else if(array[0] == null)
			return array;
			else {
				List nonEmptyRows = new ArrayList();
				for(int i = 0; i < array.length; i++) {
					boolean rowToDelete = false;
					if(array[i] instanceof IData) {
						rowToDelete = cleanupNullIData1((IData)array[i]);
					} else {
						rowToDelete = cleanupNonIData(array[i]);
					}
					if(!rowToDelete)
					nonEmptyRows.add(array[i]);
				}
				if(nonEmptyRows.size() == 0)
				return null;
				else {
					cleanedArray = new Object[nonEmptyRows.size()];
					Iterator iter = nonEmptyRows.iterator();
					for(int i = 0; iter.hasNext(); i++)
					cleanedArray[i] = iter.next();
				}
			}
			return cleanedArray;
		}
		
		
		private static boolean cleanupNonIData(Object obj) {
			boolean objToDelete = false;
			/* if(obj == null || obj.toString().equals("")) */
			
			if(obj == null)
			objToDelete = true;
			return objToDelete;
		}
	
	public static String getStringFromDocument(String field, IData document) throws ServiceException{
	
		if (document == null)
			return null;
	
		String value = null;
		IDataCursor c = document.getCursor();
	
		String fields[] = field.split("/");
		for (int i=0; i<fields.length; i++){
			String currentField = fields[i];
	       	if (i == fields.length - 1){ 
				if (currentField.matches("[^\\[\\]]*\\[\\d+\\]$") ) { 
					try {
						String index = currentField.substring(
							currentField.indexOf("[")+1,
							currentField.length()-1);
						currentField = currentField.substring(0,currentField.indexOf("["));
						String[] values = IDataUtil.getStringArray(c,currentField);
						int ind = Integer.parseInt(index);
						try {
							value = values[ind];
						}	
						catch (Exception e){
						}
					}catch (Exception e){
						throw new ServiceException("Field path '"+field+"' is not proper string field path.");
					}
				}
				else {	
					boolean isArray = false;
					try {
						new Integer(IDataUtil.getStringArray(c,currentField).length);
						isArray = true; 
					} catch (Exception e){
					} 
					if (isArray)
						throw new ServiceException("Field '"+field+"' is stiring array while simple string is expected." );
					value = IDataUtil.getString(c,fields[i]);
				}
				
			} else {
				if (currentField.matches("[^\\[\\]]*\\[\\d+\\]$")) { 
					int ind=-1;
					try {
						String index = currentField.substring(
							currentField.indexOf("[")+1,
							currentField.length()-1);
						currentField = currentField.substring(0,currentField.indexOf("["));
						String[] values = IDataUtil.getStringArray(c,currentField);
						ind = Integer.parseInt(index); 
					
					}catch (Exception e){
						throw new ServiceException("Field path '"+field+"' is not proper string field path.");
					}
					IData[] documents = IDataUtil.getIDataArray( c, currentField );	
					try {
						document = documents[ind];
					} catch (Exception e){
					}
					if (document == null)
						break;
					c =  document.getCursor();
				}else{
	
					document = IDataUtil.getIData( c, fields[i] );	
					if (document == null)
						break;
					c =  document.getCursor();
				}
			}
		
		}
		return value;
	}
	
			
	// --- <<IS-END-SHARED>> ---
}


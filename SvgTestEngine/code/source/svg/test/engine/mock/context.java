package svg.test.engine.mock;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.softwareag.util.IDataMap;
// --- <<IS-END-IMPORTS>> ---

public final class context

{
	// ---( internal utility methods )---

	final static context _instance = new context();

	static context _newInstance() { return new context(); }

	static context _cast(Object o) { return (context)o; }

	// ---( server methods )---




	public static final void setTestCaseContext (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(setTestCaseContext)>> ---
		// @sigtype java 3.5
		// [i] field:0:required testCaseName
		IDataMap pipelineMap = new IDataMap(pipeline);
		String testCaseName = pipelineMap.getAsString("testCaseName");
		pipelineMap.remove("testCaseName");
		currentTestCase.set(testCaseName);
		// --- <<IS-END>> ---

                
	}



	public static final void setTestSuiteContext (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(setTestSuiteContext)>> ---
		// @sigtype java 3.5
		// [i] field:0:required testSuiteName
		IDataMap pipelineMap = new IDataMap(pipeline);
		String testSuiteName = pipelineMap.getAsString("testSuiteName");
		pipelineMap.remove("testSuiteName");
		currentTestSuite.set(testSuiteName);
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	public static ThreadLocal<String> currentTestSuite = new ThreadLocal<String>();
	public static ThreadLocal<String> currentTestCase = new ThreadLocal<String>();
	// --- <<IS-END-SHARED>> ---
}


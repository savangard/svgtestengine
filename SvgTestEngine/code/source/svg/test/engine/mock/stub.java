package svg.test.engine.mock;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.Session;
import com.wm.app.b2b.server.User;
import com.wm.util.coder.IDataXMLCoder;
import com.wm.util.coder.InvalidDatatypeException;
import com.wm.lang.ns.NSName;
import com.wm.lang.ns.NSService;
import com.wm.util.JournalLogger;
import com.wm.util.ServerException;
import com.softwareag.util.IDataMap;
import com.wm.app.b2b.server.BaseService;
import com.wm.app.b2b.server.ISRuntimeException;
import com.wm.app.b2b.server.InvokeState;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Stack;
import com.wm.app.b2b.server.invoke.InvokeChainProcessor;
import com.wm.app.b2b.server.invoke.InvokeManager;
import com.wm.app.b2b.server.invoke.ServiceStatus;
import com.wm.app.b2b.server.ns.Namespace;
// --- <<IS-END-IMPORTS>> ---

public final class stub

{
	// ---( internal utility methods )---

	final static stub _instance = new stub();

	static stub _newInstance() { return new stub(); }

	static stub _cast(Object o) { return (stub)o; }

	// ---( server methods )---




	public static final void loadStub (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(loadStub)>> ---
		// @sigtype java 3.5
		// [i] field:0:required scope
		// [i] field:0:required serviceName
		// [i] object:0:required pipeline
		// [i] record:0:optional parameters
		IDataMap dataMap = new IDataMap(pipeline);
		String serviceName = dataMap.getAsString("serviceName");
		Object inputPipeline = dataMap.get("pipeline");
		String scope = dataMap.getAsString("scope");
		String testCaseContext = (String) svg.test.engine.mock.context.currentTestCase.get();
		
		if (!isScopeValid(scope)) {
			scope = InvocationStubber.scope_user;
		}
		
		Stub stub = null;
		if (inputPipeline instanceof String) {
			stub = new Stub( (String) inputPipeline, dataMap.getAsIData("parameters"));
		} else if (inputPipeline instanceof IData) {
			stub = new Stub((IData) inputPipeline);
		}
		
		InvocationStubber.setupStub(serviceName + "_" + testCaseContext, stub, scope);
		// --- <<IS-END>> ---

                
	}



	public static final void removeAllStubs (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(removeAllStubs)>> ---
		// @sigtype java 3.5
		InvocationStubber.clearAllStubs();
		// --- <<IS-END>> ---

                
	}



	public static final void removeStub (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(removeStub)>> ---
		// @sigtype java 3.5
		// [i] field:0:required scope
		// [i] field:0:required serviceName
		IDataMap dataMap = new IDataMap(pipeline);
		String serviceName = dataMap.getAsString("serviceName");
		String testCaseContext = (String) svg.test.engine.mock.context.currentTestCase.get();
		
		String scope = dataMap.getAsString("scope");
		if (!isScopeValid(scope)) {
			scope = InvocationStubber.scope_user;
		}
		
		InvocationStubber.clearStub(serviceName + "_" + testCaseContext, scope);
		// --- <<IS-END>> ---

                
	}



	public static final void restorePipeline (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(restorePipeline)>> ---
		// @sigtype java 3.5
		// [i] field:0:required path
		// [o] record:0:required pipeline
		IDataCursor cursor = pipeline.getCursor();
		String pipelinePath = IDataUtil.getString(cursor, "path");
		IDataUtil.remove(cursor, "path");
		
		IData idata = null;
		try {
			File file = new File(pipelinePath);
			IDataXMLCoder coder = new IDataXMLCoder();
			idata = coder.readFromFile(file);
			IDataUtil.put(cursor, "pipeline", idata);
		} catch (Exception e) {
			e.printStackTrace();
		}
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}



	public static final void xmlIDataStreamToDocument (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(xmlIDataStreamToDocument)>> ---
		// @sigtype java 3.5
		// [i] object:0:required IDataXMLStream
		// [o] record:0:required outputDocument
		IDataCursor cursor = pipeline.getCursor();
		InputStream inputStream = (InputStream) IDataUtil.get(cursor, "IDataXMLStream");
		cursor.destroy();
		
		IDataXMLCoder coder = new IDataXMLCoder();
		IData idata = null;
		
		try {
			idata = coder.decode(inputStream);
		} catch (InvalidDatatypeException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		cursor = pipeline.getCursor();
		IDataUtil.put(cursor, "outputDocument", idata);
		cursor.destroy();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	
	private static boolean isScopeValid(String scope) {
		return InvocationStubber.key_user.equals(scope) || InvocationStubber.scope_session.equals(scope) || InvocationStubber.scope_server.equals(scope);
	}
	
	public static class Stub implements Serializable {
		private static final long serialVersionUID = -3122776153873414108L;
		public static final int type_data = 1;
		public static final int type_service = 2;
		private int type = 1;
		private String service;
		private IData iData;
		 
		public Stub(String service, IData parms) {
			setType(type_service);
		    setServiceName(service);
		    this.iData = parms;
		} 
	
		public Stub(String service) {
		    this(service, null);
		}
	
		public Stub(IData idata) {
		    setType(type_data);
		    setIData(idata);		 
		}
		
		public IData getIData()
		{
		   return this.iData;
		}
		
		public void setIData(IData idata)
		{
			setType(type_data);
		    this.iData = idata;
		}
		
		public String getServiceName()
		{
		    return this.service;
		}
		
		public void setServiceName(String serviceName)
		{
		    setType(type_service);
		    this.service = serviceName;
		}
		
		public int getType()
		{
		    return this.type;
		}
		
		public void setType(int type)
		{
		    this.type = type;
		}
		  
		public String toString() {
			
			String s = "";
			switch(type) {
			
				case type_service: 
					s = "[service]: " + service;
					break;
				case type_data:
					s = "[pipeline]";
					break;
			}
			
			return s;
		}
	}
	
	
	public static class InvocationStubber implements InvokeChainProcessor {
			
		public static final String scope_session = "session";
		public static final String scope_user    = "user";
		public static final String scope_server  = "server";
			
		private static final String key_server   = "__server__";
		private static final String key_user     = "__user__";
	    private static final String key_session  = "__session__";
			
		private static Hashtable<String, Hashtable<String, Stub>> stubbedServicesTables = new Hashtable<String, Hashtable<String, Stub>>();
			  
		public static synchronized void setupStub(String serviceName, Stub stub, String scope) {
			if (scope_session.equalsIgnoreCase(scope)) {
				Session session = InvokeState.getCurrentSession();
	//			      JournalLogger.log(4, 
	//			    		  com.wm.util.JournalLogger.FAC_FLOW_SVC,
	//			    		  com.wm.util.JournalLogger.INFO, 
	//			    		  "[" + message_prefix + "] Session ", 
	//			    		  session);
			      
	
			    Hashtable<String, Stub> servicesToStub = (Hashtable<String, Stub>) session.get(key_session);
			    if (servicesToStub == null) {
			    	servicesToStub = new Hashtable<String, Stub>();
			        session.put(key_session, servicesToStub);
			    }
	
			    servicesToStub.put(serviceName, stub);
			    //log(MessageFormat.format("Stub for {1} in {2} as service {3}", new Object[] { serviceName, scope, stub }));
			    return;
			}
	
			String key = key_server;
			if (scope_user.equalsIgnoreCase(scope)) {
				key = InvokeState.getCurrentUser().getName();
			}
	
			Hashtable<String, Stub> servicesToIntercept = (Hashtable<String, Stub>)stubbedServicesTables.get(key);
			
			if (servicesToIntercept == null) {
				servicesToIntercept = new Hashtable<String, Stub>();
			    stubbedServicesTables.put(key, servicesToIntercept);
			 }
			
			if (key == key_server) {
				String[] serviceAndContext = serviceName.split("_");
				servicesToIntercept.put(serviceAndContext[0], stub);
			} else {
				servicesToIntercept.put(serviceName, stub);
			}
			 
			 //log(MessageFormat.format("Stub for {1} in {2} as service {3}", new Object[] { serviceName, scope, stub }));
		      
			 JournalLogger.log(4, 
		    		  com.wm.util.JournalLogger.FAC_FLOW_SVC,
		    		  com.wm.util.JournalLogger.INFO, 
		    		  "[" + message_prefix + "] current session ", 
		    		  InvokeState.getCurrentSession());
		}
			
		public static synchronized void clearStub(String serviceName, String scope) {
			if (scope_session.equalsIgnoreCase(scope)) {
				Session session = InvokeState.getCurrentSession();
			    Hashtable<String, Stub> servicesToIntercept = (Hashtable<String, Stub>)session.get(key_session);
			    if (servicesToIntercept != null) {
			    	//log(MessageFormat.format("Removed stub (session) for {1}", new Object[] { serviceName.toString() }));
			        servicesToIntercept.remove(serviceName);
			    }
			} else {
				String key = key_server;
	
			    if (scope_user.equalsIgnoreCase(scope))
			    {
			        key = InvokeState.getCurrentUser().getName();
			    } else {
					if (key == key_server) {
						String[] serviceAndContext = serviceName.split("_");
						serviceName = serviceAndContext[0];
			    }
			    
			      Hashtable<String, Stub> servicesToIntercept = (Hashtable<String, Stub>) stubbedServicesTables.get(key);
			      if (servicesToIntercept != null)
			      {
			    	  //log(MessageFormat.format("Removed stub (server) for {1}", new Object[] { serviceName.toString() }));
			    	  servicesToIntercept.remove(serviceName);
			      }
			    }
			}
		}
			  
			public static synchronized void clearAllStubs() {
			    InvokeState.getCurrentSession().remove(key_session);
			    stubbedServicesTables.clear();
			}
			
			@Override
			public void process(Iterator chain, BaseService baseService, IData pipeline, ServiceStatus status) throws ServerException {
				String serviceName = baseService.getNSName().getFullName();
				String testCaseContext = (String) svg.test.engine.mock.context.currentTestCase.get();
				Stub stub = getStubObject(serviceName + "_" + testCaseContext);
				
	//			    log(MessageFormat.format("Search stub for context {1} as {2}" , new Object[] { testCaseContext, baseService.getNSName().getFullName() }));
			    
	//				 JournalLogger.log(4, 
	//			    		  com.wm.util.JournalLogger.FAC_FLOW_SVC,
	//			    		  com.wm.util.JournalLogger.INFO, 
	//			    		  "[" + message_prefix + "] current context ", 
	//			    		  testCaseContext);
	//				 
	//				 JournalLogger.log(4, 
	//			    		  com.wm.util.JournalLogger.FAC_FLOW_SVC,
	//			    		  com.wm.util.JournalLogger.INFO, 
	//			    		  "[" + message_prefix + "] current service ", 
	//			    		  serviceName);
			    
			    if (stub != null)
			    	{
			    		IData stubPipeline;
			    		switch (stub.getType())
			    		{
				    		case 1:
			    				log(MessageFormat.format(message_pipelineStub, new Object[] { serviceName }));
	
			    				stubPipeline = stub.getIData();
			    				IDataUtil.merge(stubPipeline, pipeline);
			    				status.setReturnValue(pipeline);
			    				return;
			    			case 2:
			    				if (isRecursive(serviceName))
			    				{
			    					log(MessageFormat.format(message_recursiveStub, new Object[] { serviceName }));
			    				}
			    				else
			    				{
			    					String stubService = stub.getServiceName();
			    					if (stubService != null)
			    					{
			    						log(MessageFormat.format(message_serviceStub, new Object[] { serviceName, stubService }));
			    						IData serviceParms = stub.getIData();
			    						if (serviceParms != null)
			    						{
			    							IDataUtil.merge(serviceParms, pipeline);
			    						}
	
			    						baseService = Namespace.getService(NSName.create(stubService)); 
			    					} 
			    				}
			    				break;
			      
			    			default:
			    				throw new ServerException(MessageFormat.format(message_unrecognizedStub, new Object [] {stub.getClass().getName()})); 			     			       
			    		}
			    	}
			
			    if (chain.hasNext())
				{
			    	((InvokeChainProcessor)chain.next()).process(chain, baseService, pipeline, status);
				}
			}
			
			private Stub getStubObject(String serviceName) {
			    Session session = InvokeState.getCurrentSession();
			    Stub stub = null;			    
			    
			    if ((session != null) && (session.containsKey(key_session))) {
			      	
			      Hashtable <String, Stub> servicesTable = (Hashtable<String, Stub>) session.get(key_session);
			      Enumeration<String> keys = servicesTable.keys();
			      while(keys.hasMoreElements()) {
			    	  String key = keys.nextElement();					    	  
			      }
			      stub =  servicesTable.get(serviceName);
			    }
	
			    if (stub == null) {
			      User user = InvokeState.getCurrentUser();
			      
			      if (user != null) {
			        Hashtable<String, Stub> servicesTable = (Hashtable<String, Stub>)stubbedServicesTables.get(user.getName());
			        if (servicesTable != null) {			        	
			        	stub = (Stub) servicesTable.get(serviceName);
			        }
			      }
	
			      if (stub == null) {
			        Hashtable<String, Stub> servicesTable = (Hashtable<String, Stub>)stubbedServicesTables.get(key_server);
			        if (servicesTable != null) {
					  String[] serviceAndContext = serviceName.split("_");
			          stub = (Stub) servicesTable.get(serviceAndContext[0]);
			        }
			      }
			    }
	//			    if (stub != null) {
	//			    	log(MessageFormat.format("Get stub for context {1} as {2}" , new Object[] { svg.test.engine.mock.context.currentTestCase.get(), stub.getServiceName() }));   
	//				}
			    return stub;
			  }
			
			public static Object[] getStubbedServices() {
			    Session session = InvokeState.getCurrentSession();
			    Object[] sessionServices = null;
			    Object[] userServices = null;
			    Object[] globalServices = null;
	
			    if ((session != null) && (session.containsKey(key_session)))
			    {
			      Hashtable<String, Stub> servicesTable = (Hashtable<String, Stub>) session.get(key_session);
			      if (servicesTable != null) {
				      sessionServices = new Stub[servicesTable.size()];
				      sessionServices = (String[])servicesTable.keySet().toArray(new String[0]);
			      }
			      
			    }
	
			    User user = InvokeState.getCurrentUser();
	
			    if (user != null)
			    {
			      Hashtable<String, Stub> servicesTable = (Hashtable<String, Stub>)stubbedServicesTables.get(user.getName());
			      if (servicesTable != null)
			      {
			    	  userServices = new Stub[servicesTable.size()];
			    	  sessionServices = (String[])servicesTable.keySet().toArray(new String[0]);
			      }
			    }
	
			    Hashtable<String, Stub> servicesTable = (Hashtable<String, Stub>)stubbedServicesTables.get(key_server);
			    if (servicesTable != null)
			    {
			    	globalServices = new Stub[servicesTable.size()];
			        globalServices = (String[])servicesTable.keySet().toArray(new String[0]);
			    }
	
			    return new Object[] { globalServices, userServices, sessionServices };
			}			
			
			private boolean isRecursive(String serviceName)
			  {
			    InvokeState state = InvokeState.getCurrentState();
			    Stack<NSService> callStack = state.getCallStack();
			    int index = -1;
			    for (int i = 0; i < callStack.size(); i++)
			    {
			      NSService svcEntry = (NSService)callStack.elementAt(i);
			      if (svcEntry.getNSName().getFullName().equals(serviceName))
			      {
			        index = i;
			        break;
			      }
			    }
			    
		
			    return ((index >= 0) && (index < callStack.size() - 1));
			    
			  }
		}
		
	
	private static void log(String message) {
	      JournalLogger.log(4, 
	    		  com.wm.util.JournalLogger.FAC_FLOW_SVC,
	    		  com.wm.util.JournalLogger.INFO, 
	    		  "[" + message_prefix + "]", 
	    		  message);
	}
	    
	private static final String message_prefix                       = "SVG - InvocationStubber";
	private static final String message_pipelineStub                 = "Service {0} stubbed by pipeline.";
	private static final String message_serviceStub                  = "Service {0} stubbed by alternate service {1}";
	private static final String message_recursiveStub                = "Recursive stub for service {0} ignored";
	private static final String message_unrecognizedStub             = "Unrecognized stub object";
		
	// --- <<IS-END-SHARED>> ---
}


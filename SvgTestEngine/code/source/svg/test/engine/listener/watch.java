package svg.test.engine.listener;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.InvokeState;
import com.wm.app.b2b.server.invoke.InvokeChainProcessor;
import com.wm.app.b2b.server.invoke.InvokeManager;
import com.wm.app.b2b.server.invoke.ServiceStatus;
import com.wm.app.b2b.server.ns.Namespace;
import com.wm.app.b2b.server.BaseService;
import com.wm.lang.ns.NSName;
import com.wm.lang.ns.NSService;
import com.wm.util.ServerException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;
import java.io.IOException;
// --- <<IS-END-IMPORTS>> ---

public final class watch

{
	// ---( internal utility methods )---

	final static watch _instance = new watch();

	static watch _newInstance() { return new watch(); }

	static watch _cast(Object o) { return (watch)o; }

	// ---( server methods )---




	public static final void shutdown (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(shutdown)>> ---
		// @sigtype java 3.5
		unregisterProcessor();
		// --- <<IS-END>> ---

                
	}



	public static final void startWatchingCallbacks (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(startWatchingCallbacks)>> ---
		// @sigtype java 3.5
		// [i] field:0:required serviceName {""}
		// [i] field:0:required event {"start","end","success","error"}
		// [i] field:0:required callback {""}
		IDataCursor cursor = pipeline.getCursor();
		String emitterName = (String)IDataUtil.get(cursor, "serviceName");
		String eventName = (String)IDataUtil.get(cursor, "event");
		String listener = (String)IDataUtil.get(cursor, "callback");
		
		Namespace ns = Namespace.current();
		NSName emitterNameNS = NSName.create(emitterName);
		NSName listenerNameNS = NSName.create(listener);
		
		if (!ns.nodeExists(emitterNameNS)) {
			throw new ServiceException("Cannot register callback " + listener + " on " + eventName + " of " + emitterName + ": " + emitterName +" does not exist");
		}
		if ( !(ns.getNode(emitterNameNS) instanceof NSService)) {
			throw new ServiceException("Cannot register callback " + listener + " on " + eventName + " of " + emitterName + ": " + emitterName +" is not a service");
		}
		if (!ns.nodeExists(listenerNameNS)) {
			throw new ServiceException("Cannot register callback " + listener + " on " + eventName + " of " + emitterName + ": " + listener +" does not exist");
		}
		if ( !(ns.getNode(listenerNameNS) instanceof NSService)) {
			throw new ServiceException("Cannot register callback " + listener + " on " + eventName + " of " + emitterName + ": " + listener +" is not a service");
		}
		
		try {
		   InvocationEventEmitter.Event.valueOf(eventName);
		} catch (IllegalArgumentException iae) {
			throw new ServiceException("Cannot register callback " + listener + " on " + eventName + " of " + emitterName + ": " + eventName +" is not a valid event");
		}
		
		HashMap<String, ArrayList> emitter = emitters.get(emitterName);
		if (emitter == null) {
			emitter = new HashMap<String, ArrayList>();
			emitters.put(emitterName, emitter);
		}
		
		ArrayList<String> listeners = emitter.get(eventName);
		if (listeners == null) {
			listeners = new ArrayList<String>();
			emitter.put(eventName, listeners);
		}
		
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
		// --- <<IS-END>> ---

                
	}



	public static final void startup (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(startup)>> ---
		// @sigtype java 3.5
		registerProcessor();
		// --- <<IS-END>> ---

                
	}



	public static final void stopWatchingCallbacks (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(stopWatchingCallbacks)>> ---
		// @sigtype java 3.5
		// [i] field:0:required serviceName
		// [i] field:0:optional event {"start","end","success","error"}
		// [i] field:0:optional callback
		IDataCursor cursor = pipeline.getCursor();
		String emitterName = (String)IDataUtil.get(cursor, "serviceName");
		String eventName = (String)IDataUtil.get(cursor, "event");
		String listener = (String)IDataUtil.get(cursor, "callback");
		
		if (eventName == null || eventName.isEmpty()) {
			emitters.remove(emitterName);
			return;
		}
		
		HashMap<String, ArrayList> emitter = emitters.get(emitterName);
		if (emitter == null) {
			return;
		}
		
		if (listener == null || listener.isEmpty()) {
			emitter.remove(eventName);
			return;
		}
		
		ArrayList<String> listeners = emitter.get(eventName);
		if (listeners == null) {
			return;
		}
		
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
		
		if (listeners.size() == 0) {
			emitter.remove(eventName);
		}
		
		if (emitter.size() == 0) {
			emitters.remove(emitterName);
		}
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	
	public static boolean enabled = false;
	private static InvokeChainProcessor invocationEventEmitter = new InvocationEventEmitter();
	private static HashMap<String, HashMap> emitters = new HashMap<String, HashMap>();
	
	public static void registerProcessor() {
		if (!enabled) {
			enabled = true;
			InvokeManager.getDefault().registerProcessor(invocationEventEmitter);
		}
		
		com.wm.util.JournalLogger.log(
					4,
		            com.wm.util.JournalLogger.FAC_FLOW_SVC,
		            com.wm.util.JournalLogger.INFO,
		            "[SVG - InvocationEventEmitter]",
		            "enabled" 
	            );
	}
	
	public static void unregisterProcessor() {
		InvokeManager.getDefault().unregisterProcessor(invocationEventEmitter);
		enabled = false;
		com.wm.util.JournalLogger.log(
					4,
		            com.wm.util.JournalLogger.FAC_FLOW_SVC,
		            com.wm.util.JournalLogger.INFO,
		            "[SVG - InvocationEventEmitter]",
		            "disabled" 
	            );
	}
	
	public static void emitInvokeEvent(String emitterName, String eventName, IData data, String message) {
		emitInvokeEvent(emitterName, eventName, data, message, null);
	}
	
	public static void emitInvokeEvent(String emitterName, String eventName, IData data, String message, Exception exception) {
		HashMap<String, ArrayList> emitter = emitters.get(emitterName);
		if (emitter == null) {
			return;
		}
		
		ArrayList<String> listeners = emitter.get(eventName);
		if (listeners == null) {
			return;
		}
		
		for (int i = 0; i < listeners.size(); i++) {
			String listener = listeners.get(i);
			com.wm.util.JournalLogger.log(
						4,
			            com.wm.util.JournalLogger.FAC_FLOW_SVC,
			            com.wm.util.JournalLogger.VERBOSE1,
			            "[SVG - InvocationEventEmitter]",    
			            "Emitting " + emitterName + "::" + eventName + " to:  " + listener
		            );
	
			IData listenerData = null;
			try {
				listenerData = IDataUtil.deepClone(data);
			} catch (IOException ioe) {
				listenerData = IDataFactory.create(new Object[][] {
					{ "message", "SVG InvokeListener [emitInvokeEvent]: error while cloning service pipeline: " + ioe.getMessage() }
				});
			}
			
			IData listenerPipeline = IDataFactory.create(new Object[][] {
					{ "emitter", emitterName },
					{ "message", message },
					{ "data", listenerData },
					{ "event", eventName }
			});
			IDataCursor cursor = listenerPipeline.getCursor();
			
			if (exception != null) {
				IDataUtil.put(cursor, "exception", exception);
			}
			
			try {
		        NSName listenerNS = NSName.create(listener);
		        Service.doThreadInvoke(listenerNS, InvokeState.getCurrentState().getSession(), listenerPipeline);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			i++;
		}
	}
	
	public static class InvocationEventEmitter implements com.wm.app.b2b.server.invoke.InvokeChainProcessor {
		public void process(Iterator chain, BaseService baseService, IData pipeline, ServiceStatus status) 
				throws ServerException 
		{ 
			try {
				emitInvokeEvent(baseService.toString(), "start", pipeline, "Service Invocation Start");
			} catch (Exception e) {
				e.printStackTrace(); 
				throw new ServiceException(e);
			}
				
			if (chain.hasNext()) {
				try {
					((InvokeChainProcessor)chain.next()).process(chain, baseService, pipeline, status);
					emitInvokeEvent(baseService.toString(), "success", pipeline, "Service Invocation Success");
										
				} catch (ServerException e) {
					emitInvokeEvent(baseService.toString(), "error", pipeline, e.getMessage(), e);
					throw e;
				} finally {
					emitInvokeEvent(baseService.toString(), "end", pipeline, "Service Invocation End");
				}
			}
		}
		
		public final static String start = "start";
		
		public static enum Event {
			start,
			end,
			success,
			error;
			
		}
	}
	// --- <<IS-END-SHARED>> ---
}


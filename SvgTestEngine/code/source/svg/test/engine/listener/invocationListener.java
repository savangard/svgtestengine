package svg.test.engine.listener;

// -----( IS Java Code Template v1.2

import com.wm.data.*;
import com.wm.util.Values;
import com.wm.app.b2b.server.Service;
import com.wm.app.b2b.server.ServiceException;
// --- <<IS-START-IMPORTS>> ---
import com.wm.app.b2b.server.invoke.InvokeChainProcessor;
import com.wm.app.b2b.server.invoke.InvokeManager;
// --- <<IS-END-IMPORTS>> ---

public final class invocationListener

{
	// ---( internal utility methods )---

	final static invocationListener _instance = new invocationListener();

	static invocationListener _newInstance() { return new invocationListener(); }

	static invocationListener _cast(Object o) { return (invocationListener)o; }

	// ---( server methods )---




	public static final void registerInvocationListener (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(registerInvocationListener)>> ---
		// @sigtype java 3.5
		registerProcessor();
		// --- <<IS-END>> ---

                
	}



	public static final void unregisterInvocationListener (IData pipeline)
        throws ServiceException
	{
		// --- <<IS-START(unregisterInvocationListener)>> ---
		// @sigtype java 3.5
		unregisterProcessor();
		// --- <<IS-END>> ---

                
	}

	// --- <<IS-START-SHARED>> ---
	public static boolean enabled = false;
	 
	private static InvokeChainProcessor invocationStubber = new svg.test.engine.mock.stub.InvocationStubber();
	
	public static void registerProcessor() {
		if (!enabled) {
		
			InvokeManager.getDefault().registerProcessor(invocationStubber);
		}
		enabled = true;
		com.wm.util.JournalLogger.log(4,
	            com.wm.util.JournalLogger.FAC_FLOW_SVC,
	            com.wm.util.JournalLogger.INFO,
	            "[SVG - InvocationStubber]",
	            "enabled");
	}
	
	public static void unregisterProcessor() {
		InvokeManager.getDefault().unregisterProcessor(invocationStubber);
		enabled = false;
		com.wm.util.JournalLogger.log(4,
	            com.wm.util.JournalLogger.FAC_FLOW_SVC,
	            com.wm.util.JournalLogger.INFO,
	            "[SVG - InvocationStubber]"
	            ,"disabled"  );
	}
	// --- <<IS-END-SHARED>> ---
}


<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">removeStub</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFNYXAgZGF0YU1hcCA9IG5ldyBJRGF0YU1hcChwaXBlbGluZSk7DQpTdHJpbmcgc2Vydmlj
ZU5hbWUgPSBkYXRhTWFwLmdldEFzU3RyaW5nKCJzZXJ2aWNlTmFtZSIpOw0KU3RyaW5nIHRlc3RD
YXNlQ29udGV4dCA9IChTdHJpbmcpIHN2Zy50ZXN0LmVuZ2luZS5tb2NrLmNvbnRleHQuY3VycmVu
dFRlc3RDYXNlLmdldCgpOw0KDQpTdHJpbmcgc2NvcGUgPSBkYXRhTWFwLmdldEFzU3RyaW5nKCJz
Y29wZSIpOw0KaWYgKCFpc1Njb3BlVmFsaWQoc2NvcGUpKSB7DQoJc2NvcGUgPSBJbnZvY2F0aW9u
U3R1YmJlci5zY29wZV91c2VyOw0KfQ0KDQpJbnZvY2F0aW9uU3R1YmJlci5jbGVhclN0dWIoc2Vy
dmljZU5hbWUgKyAiXyIgKyB0ZXN0Q2FzZUNvbnRleHQsIHNjb3BlKTs=</value>
</Values>

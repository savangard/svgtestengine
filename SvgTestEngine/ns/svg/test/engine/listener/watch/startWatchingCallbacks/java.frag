<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">startWatchingCallbacks</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgY3Vyc29yID0gcGlwZWxpbmUuZ2V0Q3Vyc29yKCk7DQpTdHJpbmcgZW1pdHRl
ck5hbWUgPSAoU3RyaW5nKUlEYXRhVXRpbC5nZXQoY3Vyc29yLCAic2VydmljZU5hbWUiKTsNClN0
cmluZyBldmVudE5hbWUgPSAoU3RyaW5nKUlEYXRhVXRpbC5nZXQoY3Vyc29yLCAiZXZlbnQiKTsN
ClN0cmluZyBsaXN0ZW5lciA9IChTdHJpbmcpSURhdGFVdGlsLmdldChjdXJzb3IsICJjYWxsYmFj
ayIpOw0KDQpOYW1lc3BhY2UgbnMgPSBOYW1lc3BhY2UuY3VycmVudCgpOw0KTlNOYW1lIGVtaXR0
ZXJOYW1lTlMgPSBOU05hbWUuY3JlYXRlKGVtaXR0ZXJOYW1lKTsNCk5TTmFtZSBsaXN0ZW5lck5h
bWVOUyA9IE5TTmFtZS5jcmVhdGUobGlzdGVuZXIpOw0KDQppZiAoIW5zLm5vZGVFeGlzdHMoZW1p
dHRlck5hbWVOUykpIHsNCgl0aHJvdyBuZXcgU2VydmljZUV4Y2VwdGlvbigiQ2Fubm90IHJlZ2lz
dGVyIGNhbGxiYWNrICIgKyBsaXN0ZW5lciArICIgb24gIiArIGV2ZW50TmFtZSArICIgb2YgIiAr
IGVtaXR0ZXJOYW1lICsgIjogIiArIGVtaXR0ZXJOYW1lICsiIGRvZXMgbm90IGV4aXN0Iik7DQp9
DQppZiAoICEobnMuZ2V0Tm9kZShlbWl0dGVyTmFtZU5TKSBpbnN0YW5jZW9mIE5TU2VydmljZSkp
IHsNCgl0aHJvdyBuZXcgU2VydmljZUV4Y2VwdGlvbigiQ2Fubm90IHJlZ2lzdGVyIGNhbGxiYWNr
ICIgKyBsaXN0ZW5lciArICIgb24gIiArIGV2ZW50TmFtZSArICIgb2YgIiArIGVtaXR0ZXJOYW1l
ICsgIjogIiArIGVtaXR0ZXJOYW1lICsiIGlzIG5vdCBhIHNlcnZpY2UiKTsNCn0NCmlmICghbnMu
bm9kZUV4aXN0cyhsaXN0ZW5lck5hbWVOUykpIHsNCgl0aHJvdyBuZXcgU2VydmljZUV4Y2VwdGlv
bigiQ2Fubm90IHJlZ2lzdGVyIGNhbGxiYWNrICIgKyBsaXN0ZW5lciArICIgb24gIiArIGV2ZW50
TmFtZSArICIgb2YgIiArIGVtaXR0ZXJOYW1lICsgIjogIiArIGxpc3RlbmVyICsiIGRvZXMgbm90
IGV4aXN0Iik7DQp9DQppZiAoICEobnMuZ2V0Tm9kZShsaXN0ZW5lck5hbWVOUykgaW5zdGFuY2Vv
ZiBOU1NlcnZpY2UpKSB7DQoJdGhyb3cgbmV3IFNlcnZpY2VFeGNlcHRpb24oIkNhbm5vdCByZWdp
c3RlciBjYWxsYmFjayAiICsgbGlzdGVuZXIgKyAiIG9uICIgKyBldmVudE5hbWUgKyAiIG9mICIg
KyBlbWl0dGVyTmFtZSArICI6ICIgKyBsaXN0ZW5lciArIiBpcyBub3QgYSBzZXJ2aWNlIik7DQp9
DQoNCnRyeSB7DQogICBJbnZvY2F0aW9uRXZlbnRFbWl0dGVyLkV2ZW50LnZhbHVlT2YoZXZlbnRO
YW1lKTsNCn0gY2F0Y2ggKElsbGVnYWxBcmd1bWVudEV4Y2VwdGlvbiBpYWUpIHsNCgl0aHJvdyBu
ZXcgU2VydmljZUV4Y2VwdGlvbigiQ2Fubm90IHJlZ2lzdGVyIGNhbGxiYWNrICIgKyBsaXN0ZW5l
ciArICIgb24gIiArIGV2ZW50TmFtZSArICIgb2YgIiArIGVtaXR0ZXJOYW1lICsgIjogIiArIGV2
ZW50TmFtZSArIiBpcyBub3QgYSB2YWxpZCBldmVudCIpOw0KfQ0KDQpIYXNoTWFwPFN0cmluZywg
QXJyYXlMaXN0PiBlbWl0dGVyID0gZW1pdHRlcnMuZ2V0KGVtaXR0ZXJOYW1lKTsNCmlmIChlbWl0
dGVyID09IG51bGwpIHsNCgllbWl0dGVyID0gbmV3IEhhc2hNYXA8U3RyaW5nLCBBcnJheUxpc3Q+
KCk7DQoJZW1pdHRlcnMucHV0KGVtaXR0ZXJOYW1lLCBlbWl0dGVyKTsNCn0NCg0KQXJyYXlMaXN0
PFN0cmluZz4gbGlzdGVuZXJzID0gZW1pdHRlci5nZXQoZXZlbnROYW1lKTsNCmlmIChsaXN0ZW5l
cnMgPT0gbnVsbCkgew0KCWxpc3RlbmVycyA9IG5ldyBBcnJheUxpc3Q8U3RyaW5nPigpOw0KCWVt
aXR0ZXIucHV0KGV2ZW50TmFtZSwgbGlzdGVuZXJzKTsNCn0NCg0KaWYgKCFsaXN0ZW5lcnMuY29u
dGFpbnMobGlzdGVuZXIpKSB7DQoJbGlzdGVuZXJzLmFkZChsaXN0ZW5lcik7DQp9</value>
</Values>

<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">getMatchingStrings</value>
  <value name="encodeutf8">true</value>
  <value name="body">SURhdGFDdXJzb3IgaW5DdXJzb3IgPSBwaXBlbGluZS5nZXRDdXJzb3IoKTsNClN0cmluZ1tdIGlu
cHV0TGlzdCA9IElEYXRhVXRpbC5nZXRTdHJpbmdBcnJheShpbkN1cnNvciwgImlucHV0TGlzdCIp
Ow0KU3RyaW5nIHJlZ2V4ID0gKFN0cmluZykgSURhdGFVdGlsLmdldFN0cmluZyhpbkN1cnNvciwg
InJlZ2V4Iik7DQppbkN1cnNvci5kZXN0cm95KCk7ICANCg0KU2V0PFN0cmluZz4gaW5wdXRTZXQg
PSBTdHJlYW0ub2YoaW5wdXRMaXN0KS5jb2xsZWN0KENvbGxlY3RvcnMudG9TZXQoKSk7DQpBcnJh
eUxpc3Q8U3RyaW5nPiBtYXRjaGVzID0gbmV3IEFycmF5TGlzdDxTdHJpbmc+KCk7DQoNCmZvciAo
U3RyaW5nIHM6aW5wdXRTZXQpIHsNCglpZiAoUGF0dGVybi5tYXRjaGVzKHJlZ2V4LCBzKSkgew0K
CQltYXRjaGVzLmFkZChzKTsNCiAgICB9DQp9DQoNCmlmKCFtYXRjaGVzLmlzRW1wdHkoKSkgew0K
CVN0cmluZ1tdIG91dHB1dExpc3QgPSBtYXRjaGVzLnRvQXJyYXkobmV3IFN0cmluZ1ttYXRjaGVz
LnNpemUoKV0pOw0KCUlEYXRhQ3Vyc29yIG91dHB1dEN1cnNvciA9IHBpcGVsaW5lLmdldEN1cnNv
cigpOw0KCUlEYXRhVXRpbC5wdXQob3V0cHV0Q3Vyc29yLCAib3V0cHV0TGlzdCIsIG91dHB1dExp
c3QpOw0KfQ==</value>
</Values>

<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">isNotEqual</value>
  <value name="encodeutf8">true</value>
  <value name="body">dHJ5IHsNCglEYXRhV3JhcHBlciB3cmFwcGVyID0gRGF0YVdyYXBwZXIuZnJvbVBpcGVsaW5lKHBp
cGVsaW5lKTsNCglPYmplY3QgYWN0dWFsRGF0YSA9IHdyYXBwZXIuZ2V0QWN0dWFsKCk7DQoJYm9v
bGVhbiByZXMgPSAhT2JqZWN0cy5lcXVhbHMoYWN0dWFsRGF0YSwgd3JhcHBlci5nZXRWYWx1ZSgp
KTsNCglhZGRBc3NlcnRpb25SZXN1bHRUb1BpcGVsaW5lKHBpcGVsaW5lLCByZXMsICJFeHBlY3Rl
ZCBvYmVqY3QgaXMgZXF1YWwgdG8gQWN0dWFsIHdoZW4gaXQgc2hvdWxkbid0IGJlLiIpOw0KfSBj
YXRjaCAoVGhyb3dhYmxlIHQpIHsNCglhZGRFcnJvck1lc3NhZ2VUb1BpcGVsaW5lKHBpcGVsaW5l
LCB0KTsgDQp9DQoJ</value>
</Values>

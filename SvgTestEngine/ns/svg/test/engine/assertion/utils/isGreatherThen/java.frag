<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">isGreatherThen</value>
  <value name="encodeutf8">true</value>
  <value name="body">dHJ5IHsNCglEYXRhV3JhcHBlciB3cmFwcGVyID0gRGF0YVdyYXBwZXIuZnJvbVBpcGVsaW5lKHBp
cGVsaW5lKTsNCglPYmplY3QgYWN0dWFsRGF0YSA9IHdyYXBwZXIuZ2V0QWN0dWFsKCk7DQoJDQoJ
aWYgKGFjdHVhbERhdGEgPT0gbnVsbCl7DQoJCWFkZEZhaWx1cmVFcnJvck1lc3NhZ2VUb1BpcGVs
aW5lKHBpcGVsaW5lLCAiRXhwZWN0ZWQgbm90IG51bGwgdmFsdWUiKTsNCgkJcmV0dXJuOw0KCX0N
CglCaWdEZWNpbWFsIGJpZ0RlYyA9IGFzQmlnRGVjaW1hbChhY3R1YWxEYXRhKTsNCglpZiAoYmln
RGVjICE9IG51bGwpIHsNCgkJQmlnRGVjaW1hbCB2YWx1ZSA9IGJpZ0RlYzsNCgkJQmlnRGVjaW1h
bCBleHBlY3RlZFZhbHVlID0gb2JqZWN0VG9CaWdEZWNpbWFsKHdyYXBwZXIuZ2V0VmFsdWUoKSk7
DQoJCWJvb2xlYW4gcmVzdWx0ID0gdmFsdWUuY29tcGFyZVRvKGV4cGVjdGVkVmFsdWUpID09IDE7
DQoJCWFkZEFzc2VydGlvblJlc3VsdFRvUGlwZWxpbmUocGlwZWxpbmUsIHJlc3VsdCwgIkFjdHVh
bCB2YWx1ZSBpcyAiICsgdmFsdWUgKyAiLCBleHBlY3RlZCBncmVhdGhlciB0aGVuICIgKyBleHBl
Y3RlZFZhbHVlKTsNCgl9IGVsc2UgaWYgKGFjdHVhbERhdGEuZ2V0Q2xhc3MoKS5pc0FycmF5KCkp
IHsNCgkJaW50IGV4cGVjdGVkTGVuZ2h0ID0gSW50ZWdlci5wYXJzZUludCh3cmFwcGVyLmdldFZh
bHVlKCkpOw0KCQlPYmplY3RbXSBkYXRhcyA9IChPYmplY3RbXSkgYWN0dWFsRGF0YTsNCgkJYm9v
bGVhbiByZXN1bHQgPSBkYXRhcy5sZW5ndGggPiBleHBlY3RlZExlbmdodDsNCgkJYWRkQXNzZXJ0
aW9uUmVzdWx0VG9QaXBlbGluZShwaXBlbGluZSwgcmVzdWx0LCAiQWN0dWFsIGFycmF5IHNpemUg
aXMgIiArIGRhdGFzLmxlbmd0aCArICIsIGV4cGVjdGVkIGdyZWF0aGVyIHRoZW4gIiArIGV4cGVj
dGVkTGVuZ2h0KTsNCgl9IGVsc2UgaWYgKGFjdHVhbERhdGEgaW5zdGFuY2VvZiBTdHJpbmcpIHsN
CgkJU3RyaW5nIHZhbHVlID0gKFN0cmluZykgYWN0dWFsRGF0YTsNCgkJaW50IGFjdHVhbExlbmd0
aCA9IHZhbHVlLmxlbmd0aCgpOw0KCQlpbnQgZXhwZWN0ZWRMZW5naHQgPSBJbnRlZ2VyLnBhcnNl
SW50KHdyYXBwZXIuZ2V0VmFsdWUoKSk7DQoJCWJvb2xlYW4gcmVzID0gYWN0dWFsTGVuZ3RoID4g
ZXhwZWN0ZWRMZW5naHQ7DQoJCWFkZEFzc2VydGlvblJlc3VsdFRvUGlwZWxpbmUocGlwZWxpbmUs
IHJlcywgIkFjdHVhbCBzdHJpbmcgbGVuZ3RoIGlzICIgKyBhY3R1YWxMZW5ndGggKyAiLCBleHBl
Y3RlZCBncmVhdGhlciB0aGVuICIgKyBleHBlY3RlZExlbmdodCk7DQoJfSBlbHNlIHsNCgkJYWRk
RmFpbHVyZUVycm9yTWVzc2FnZVRvUGlwZWxpbmUocGlwZWxpbmUsIGFjdHVhbERhdGEuZ2V0Q2xh
c3MoKSArICIgaXMgbm90IHN1cHBvcnRlZCIpOw0KCX0NCn0gY2F0Y2ggKFRocm93YWJsZSB0KSB7
DQoJYWRkRXJyb3JNZXNzYWdlVG9QaXBlbGluZShwaXBlbGluZSwgdCk7DQp9DQoJ</value>
</Values>

<?xml version="1.0" encoding="UTF-8"?>

<Values version="2.0">
  <value name="name">isNull</value>
  <value name="encodeutf8">true</value>
  <value name="body">dHJ5IHsNCglEYXRhV3JhcHBlciB3cmFwcGVyID0gRGF0YVdyYXBwZXIuZnJvbVBpcGVsaW5lV2l0
aG91dFZhbHVlKHBpcGVsaW5lKTsNCglPYmplY3QgYWN0dWFsRGF0YSA9IHdyYXBwZXIuZ2V0QWN0
dWFsKCk7DQoJYm9vbGVhbiByZXN1bHQgPSBhY3R1YWxEYXRhID09IG51bGw7DQoJYWRkQXNzZXJ0
aW9uUmVzdWx0VG9QaXBlbGluZShwaXBlbGluZSwgcmVzdWx0LCAiRXhwZWN0ZWQgbnVsbCBidXQg
d2FzOiAiICsgYWN0dWFsRGF0YSk7DQp9IGNhdGNoIChUaHJvd2FibGUgdCkgew0KCWFkZEVycm9y
TWVzc2FnZVRvUGlwZWxpbmUocGlwZWxpbmUsIHQpOyANCn0NCgk=</value>
</Values>

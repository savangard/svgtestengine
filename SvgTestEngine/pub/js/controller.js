window.onload = function () {
    getReportsList();
}

function getReportsList()
{
    var reportsListUrl = '/rest/svg.test.resources.getReportsList';

    $.ajax({
        type: "GET",
        crossDomain: true,
        url: reportsListUrl,
        success: (response) => {
            console.log(response.reportsList);
            showReportsList(response.reportsList);
        },
        error: function (xhr, textStatus, error, data) {
            console.log(xhr)
            console.log(xhr.responseText);
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
            console.log(data)
        }
    });
    
}

function showReportsList(reportsList){
    var tbody = document.getElementById('reportsList');
    
    for(let i = 0; i < reportsList.length; i++){
        var linkName = './' + reportsList[i];
        var link = document.createElement("a"); 
        link.setAttribute("href", linkName);
        link.className = "anchorjs-link";
		link.style = "color : #009900";
        link.innerHTML = '<u>' + i + '</u>';
        
        var tr = tbody.insertRow();
        var td1 = tr.insertCell();
        var td2 = tr.insertCell();
        var td3 = tr.insertCell();
        td1.appendChild(link);
        td2.innerHTML = transformDate(reportsList[i]);
        td3.id = 'name' + i;
        readName(reportsList[i], 'name' + i);
        
        var link = document.createElement("a");        
    }
}

function transformDate(date)
{
    var correctDate = date.substr(0, 4)
               + '-' + date.substr(4, 2)
               + '-' + date.substr(6, 2)
               + ' ' + date.substr(8, 2)
               + ':' + date.substr(10, 2)
               + ':' + date.substr(12, 2);
    
	return correctDate;
}

function readName(date, id)
{
    var url = './' + date + '/resources/name.txt';
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {	
        document.getElementById(id).innerHTML = xhr.responseText;
    };
    xhr.open('GET', url);
    xhr.send();
}

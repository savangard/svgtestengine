window.onload = function () {
    loadXMLDoc();
}

function loadXMLDoc() {
   var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function() {

		if (this.readyState == 4 && this.status == 200) {
			createTableFromXml(this);
    }
  };
 
  xmlhttp.open("GET", "./resources/report.xml", true);
  xmlhttp.send();
    
}

function createTableFromXml(xml) {
    
	var xmlDoc = xml.responseXML;
	
    showInfoTable(xmlDoc);
    showSummaryTable(xmlDoc);
    showFailedSummaryTable(xmlDoc);
    showExceptionSummaryTable(xmlDoc);
    showTestSuitesTables(xmlDoc);
}

function showInfoTable(xmlDoc)
{
    var testSuite = xmlDoc.getElementsByTagName("testsuite");
    var testSuites = xmlDoc.getElementsByTagName("testSuites");
    var testNameRow = document.getElementById("testNameRow");
    var testDateRow = document.getElementById("testDateRow");
    var numberOfTestsRow = document.getElementById("numberOfTestsRow");
    var numberOfFailedTestsRow = document.getElementById("numberOfFailedTestsRow");
    var numberOfTestSuitesRow = document.getElementById("numberOfTestSuitesRow");
    var testNameColumn = testNameRow.insertCell();
    var testDateColumn = testDateRow.insertCell();
    var numberOfTestsColumn = numberOfTestsRow.insertCell();
    var numberOfFailedTestsColumn = numberOfFailedTestsRow.insertCell();
    var numberOfTestSuitesColumn = numberOfTestSuitesRow.insertCell();
    testNameColumn.innerHTML = testSuites[0].getAttribute('name');
    testDateColumn.innerHTML = transformDate(testSuites[0].getAttribute('time'));
    numberOfTestsColumn.innerHTML = testSuites[0].getAttribute('tests');
    numberOfFailedTestsColumn.innerHTML = testSuites[0].getAttribute('failures');
    numberOfTestSuitesColumn.innerHTML = testSuite.length;
}

function showSummaryTable(xmlDoc)
{
    var summaryTableTbody = document.getElementById('summaryTableTbody');
	deleteRows(summaryTableTbody);
    
    var failedCountSum = 0, testcaseCount = 0, allTime = 0, exceptionCountSum = 0, passCountSum = 0;
    var testSuite = xmlDoc.getElementsByTagName("testsuite");
    
    for (let i = 0; i < testSuite.length; i++) {
		      
        var failedCount = 0, exceptionCount = 0, passCount = 0;
		var testcase = testSuite[i].getElementsByTagName("testcase");
        var linkName = '#' + testSuite[i].getAttribute('name');
        var link = document.createElement("a");      
        link.setAttribute("href", linkName);
		link.innerHTML = '<u>' + testSuite[i].getAttribute('name') + '</u>';
		link.className = "anchorjs-link";
		link.style = "color : #009900";
		
        var newRow = summaryTableTbody.insertRow(i);
        var newcell1 = newRow.insertCell(0);
        var newcell2 = newRow.insertCell(1);
        var newcell3 = newRow.insertCell(2);
        var newcell4 = newRow.insertCell(3);
        var newcell5 = newRow.insertCell(4);   
        var newcell6 = newRow.insertCell(5); 
        var newcell7 = newRow.insertCell(6); 
        newcell1.appendChild(link);
        newcell2.innerHTML = testcase.length;    
        newcell6.innerHTML = Round(testSuite[i].getAttribute('time'), 4);
        testcaseCount += testcase.length;
        
        for(let j = 0; j < testcase.length; j++){
            
            if (testcase[j].getAttribute('status') == "FAILED"){
                failedCount += 1;
                failedCountSum += 1;
            }
            if (testcase[j].getAttribute('status') == "EXCEPTION"){
                exceptionCount += 1;
                exceptionCountSum += 1;
            }
            if (testcase[j].getAttribute('status') == "PASSED"){
                passCount += 1;
                passCountSum += 1;
            }
        }
        
        newcell3.innerHTML = failedCount;
        newcell4.innerHTML = passCount;
        newcell5.innerHTML = exceptionCount;
        
        if (failedCount == 0 && exceptionCount == 0){
            newcell7.className = "table-success";
            newcell7.innerHTML = "Pass";
        }
        else{
            newcell7.className = "table-danger";
            newcell7.innerHTML = "Failed";
        }
		
		allTime += parseFloat(testSuite[i].getAttribute("time"));
    }
    
    var rowCount = summaryTableTbody.rows.length;
    var newRow = summaryTableTbody.insertRow(rowCount);
    var newcell1 = newRow.insertCell(0);
    var newcell2 = newRow.insertCell(1);
    var newcell3 = newRow.insertCell(2);
    var newcell4 = newRow.insertCell(3);
    var newcell5 = newRow.insertCell(4);
    var newcell6 = newRow.insertCell(5);
    var newcell7 = newRow.insertCell(6);

    
    if (failedCountSum == 0 && exceptionCountSum == 0){
		newcell1.innerHTML = '';
		newcell1.className = "table-success";
		newcell2.innerHTML = testcaseCount;
		newcell2.className = "table-success";
		newcell3.innerHTML = exceptionCountSum;	
		newcell3.className = "table-success";
        newcell4.innerHTML = passCountSum;	
		newcell4.className = "table-success";
        newcell5.innerHTML = exceptionCountSum;	
		newcell5.className = "table-success";
		newcell6.innerHTML = Round(allTime,4);
		newcell6.className = "table-success";
        newcell7.className = "table-success";
        newcell7.innerHTML = "Pass";
    }
    else{
		newcell1.innerHTML = '';
		newcell1.className = "table-danger";
		newcell2.innerHTML = testcaseCount;
		newcell2.className = "table-danger";
		newcell3.innerHTML = failedCountSum;	
		newcell3.className = "table-danger";
		newcell4.innerHTML = passCountSum;	
		newcell4.className = "table-danger";
        newcell5.innerHTML = exceptionCountSum;	
		newcell5.className = "table-danger";
		newcell6.innerHTML = Round(allTime,4);
		newcell6.className = "table-danger";
        newcell7.className = "table-danger";
        newcell7.innerHTML = "Failed";
    }
    
}

function Round(n, k)
{
    var factor = Math.pow(10, k);
    return Math.round(n*factor)/factor;
}

function showFailedSummaryTable(xmlDoc)
{
    var testSuite = xmlDoc.getElementsByTagName("testsuite");
    var failureTableTbody = document.getElementById('failureTableTbody');
    var isFailure = false, failureCases = "";
	deleteRows(failureTableTbody);
	
    for (let i = 0; i < testSuite.length; i++) {     
        isFailure = false;
        failureCases = "";
        var testcase = testSuite[i].getElementsByTagName("testcase");
		var linkName = '#' + testSuite[i].getAttribute('name');
		
        for(let j = 0; j < testcase.length; j++){
            if(testcase[j].getAttribute('status') == "FAILED"){
                isFailure = true;
                if(failureCases == "")
                    failureCases += testcase[j].getAttribute('name');
                else
                    failureCases += '<br>' + testcase[j].getAttribute('name');
            }
        }
        
        if(isFailure){
            var testcase = testSuite[i].getElementsByTagName("testcase");
            var rowCount = failureTableTbody.rows.length;
            var newRow = failureTableTbody.insertRow(rowCount);
            var newcell1 = newRow.insertCell(0);
            var newcell2 = newRow.insertCell(1); 
		    var link2 = document.createElement("a");      
			link2.setAttribute("href", linkName);
			link2.innerHTML = '<u>' + testSuite[i].getAttribute('name') + '</u>';
			link2.style = "color : #009900";
            newcell1.appendChild(link2);
            newcell2.innerHTML = failureCases;
        }
            
    }
}

function showExceptionSummaryTable(xmlDoc)
{
    var testSuite = xmlDoc.getElementsByTagName("testsuite");
    var exceptionTableTbody = document.getElementById('exceptionTableTbody');
    var isException = false, exceptionCases = "";
	deleteRows(exceptionTableTbody);
	
    for (let i = 0; i < testSuite.length; i++) {     
        isException = false;
        warningCases = "";
        var testcase = testSuite[i].getElementsByTagName("testcase");
		var linkName = '#' + testSuite[i].getAttribute('name');
		
        for(let j = 0; j < testcase.length; j++){
            if(testcase[j].getAttribute('status') == "EXCEPTION"){
                isException = true;
                if(exceptionCases == "")
                    exceptionCases += testcase[j].getAttribute('name');
                else
                    exceptionCases += '<br>' + testcase[j].getAttribute('name');
            }
        }
        
        if(isException){
            var testcase = testSuite[i].getElementsByTagName("testcase");
            var rowCount = exceptionTableTbody.rows.length;
            var newRow = exceptionTableTbody.insertRow(rowCount);
            var newcell1 = newRow.insertCell(0);
            var newcell2 = newRow.insertCell(1); 
		    var link2 = document.createElement("a");      
			link2.setAttribute("href", linkName);
			link2.innerHTML =  '<u>' + testSuite[i].getAttribute('name') + '</u>';
			link2.style = "color : #009900";
            newcell1.appendChild(link2);
            newcell2.innerHTML = exceptionCases;
        }
            
    }
}

function showTestSuitesTables(xmlDoc)
{
        var testSuite = xmlDoc.getElementsByTagName("testsuite");
        var body = document.getElementById('body');
	    var isContainer = document.getElementById('container');
	    if(isContainer == null){
		var container = document.createElement('div');
		container.id = "container";
		
		for (let i = 0; i < testSuite.length; i++) {
			var div = document.createElement('div');
			div.className = "card mx-4 my-4";
			div.id = "testCaseDiv";
			
			var table = document.createElement('table');
            table.className = "table";
			table.style = "table-layout:fixed;";
            table.id = testSuite[i].getAttribute('name');

			var thead = document.createElement('thead');
			thead.className = "table-active";
			thead.style = "background-color: #d9d9d9;";
			
			var tr = thead.insertRow();
			var td1 = tr.insertCell();
			var td2 = tr.insertCell();
			var td3 = tr.insertCell();
			var td4 = tr.insertCell();
			var td5 = tr.insertCell();
			var td6 = tr.insertCell();
			td1.innerHTML = "";
			td2.innerHTML = "";
			td3.innerHTML = testSuite[i].getAttribute('name');
			td3.style = "text-align:right";
            td3.style.fontWeight = 'bold';
			td4.innerHTML = "";
			td5.innerHTML = "";
			td6.innerHTML = "";
			
			var tr = thead.insertRow();
			tr.style = "background-color:  #e6e6e6;";
			var td1 = tr.insertCell();
			var td2 = tr.insertCell();
			var td3 = tr.insertCell();
			var td4 = tr.insertCell();
			var td5 = tr.insertCell();
			var td6 = tr.insertCell();
			td1.innerHTML = "Test Case";
            td1.style.fontWeight = 'bold';
			td2.innerHTML = "Time [ms]";
            td2.style.fontWeight = 'bold';
			td3.innerHTML = "Result";
            td3.style.fontWeight = 'bold';
			td4.innerHTML = "Assertion";
            td4.style.fontWeight = 'bold';
			td5.innerHTML = "Details";
            td5.style.fontWeight = 'bold';
			td6.innerHTML = "View Pipeline";
            td6.style.fontWeight = 'bold';
			
			table.appendChild(thead);

			var tbody = document.createElement('tbody');
			table.className = "table table-striped";
			var testcase = testSuite[i].getElementsByTagName("testcase");
			
			for(let j = 0; j < testcase.length; j++){
				var tr = tbody.insertRow();
				var td1 = tr.insertCell();
				var td2 = tr.insertCell();
				var td3 = tr.insertCell();
				var td4 = tr.insertCell();
				var td5 = tr.insertCell();
				var td6 = tr.insertCell();
				td1.innerHTML = testcase[j].getAttribute('name');
				td2.innerHTML = Round(testcase[j].getAttribute('time'), 4);
				
				if (testcase[j].getAttribute('status') == "FAILED")
					td3.className = "table-danger";
				if (testcase[j].getAttribute('status') == "PASSED")
					td3.className = "table-success";
                if (testcase[j].getAttribute('status') == "EXCEPTION")
                    td3.className = "table-warning";
				
				td3.innerHTML = testcase[j].getAttribute('status');
				td4.innerHTML = testcase[j].getAttribute('assertions');

				var details = "";
                var error = testcase[j].getElementsByTagName('error');
                var failure = testcase[j].getElementsByTagName('failure');
                
                for(var k=0; k < error.length; k++)
                    details += error[k].getAttribute("message");      
                
                for(var k=0; k < failure.length; k++)
                    details += failure[k].getAttribute("message");   
                    
				td5.innerHTML = details;
                
                if(testcase[j].getElementsByTagName('pipelineXML')[0] != null && testcase[j].getElementsByTagName('pipelineXML').length > 0){
                    var button = document.createElement('button');
                    button.className = "btn btn-secondary";
                    button.setAttribute('data-toggle', 'modal');
                    button.setAttribute('data-target', '#modal' + i + '-' + j + 'XmlPipeline');
                    button.innerHTML = "XML";
                    button.style="background: rgb(102,204,102); color: white; margin-right: 10px"
                    td6.appendChild(button);

                    var div1 = document.createElement('div');
                    div1.className = "modal fade";
                    div1.id = 'modal' + i + '-' + j + 'XmlPipeline';
                    div1.setAttribute('tabindex','-1');
                    div1.setAttribute('role','dialog');
                    div1.setAttribute('aria-labelledby', 'modal' + i + '-' + j + 'XmlPipelineTitle');
                    div1.setAttribute('aria-hidden','true');

                    var div2 = document.createElement('div');
                    div2.className = "modal-dialog";
                    div2.setAttribute('role','document');

                    var div3 = document.createElement('div');
                    div3.className = "modal-content";

                    var div4 = document.createElement('div');
                    div4.className = "modal-header";

                    var header = document.createElement('h5');
                    header.className = "modal-title";
                    header.id = 'modal' + i + '-' + j + 'XmlPipelineTitle';
                    header.innerHTML = "XML Pipeline";
                    div4.appendChild(header);
                    var div4Button = document.createElement('button');
                    div4Button.setAttribute('type','button');
                    div4Button.className = "close";
                    div4Button.setAttribute('data-dismiss','modal');
                    div4Button.setAttribute('aria-label','Close');
                    var span = document.createElement('span');
                    span.setAttribute('aria-hidden', 'true');
                    span.innerHTML = "&times;";
                    div4Button.appendChild(span);
                    div4.appendChild(div4Button);
                    div3.appendChild(div4);

                    var div5 = document.createElement('div');
                    div5.className = "modal-body";
					div5.id = 'div5' + i + '-' + j + 'XmlPipeline';
					showPipeline(testcase[j].getElementsByTagName('pipelineXML')[0].innerHTML, 'div5' + i + '-' + j + 'XmlPipeline');      

                    var div6 = document.createElement('div');
                    div6.className = "modal-footer";
                    var div6Button = document.createElement('button');
                    div6Button.setAttribute('type','button');
                    div6Button.className = "btn btn-secondary";
                    div6Button.setAttribute('data-dismiss','modal');
                    div6Button.innerHTML = "Close";
					
					div3.appendChild(div5); 
                    div6.appendChild(div6Button);         
                    div3.appendChild(div6);
                    div2.appendChild(div3);
                    div1.appendChild(div2);
                    td6.appendChild(div1);
                }
				
                
                if(testcase[j].getElementsByTagName('pipelineJSON')[0] != null && testcase[j].getElementsByTagName('pipelineJSON').length > 0){
					
					console.log("button");
					
                    var button = document.createElement('button');
                    button.className = "btn btn-secondary";
                    button.setAttribute('data-toggle', 'modal');
                    button.setAttribute('data-target', '#modal' + i + '-' + j + 'JsonPipeline');
                    button.innerHTML = "JSON";
                    button.style="background: rgb(102,204,102); color: white;"
                    td6.appendChild(button);

                    var div1 = document.createElement('div');
                    div1.className = "modal fade";
                    div1.id = 'modal' + i + '-' + j + 'JsonPipeline';
                    div1.setAttribute('tabindex','-1');
                    div1.setAttribute('role','dialog');
                    div1.setAttribute('aria-labelledby', 'modal' + i + '-' + j + 'JsonPipelineTitle');
                    div1.setAttribute('aria-hidden','true');

                    var div2 = document.createElement('div');
                    div2.className = "modal-dialog";
                    div2.setAttribute('role','document');

                    var div3 = document.createElement('div');
                    div3.className = "modal-content";

                    var div4 = document.createElement('div');
                    div4.className = "modal-header";

                    var header = document.createElement('h5');
                    header.className = "modal-title";
                    header.id = 'modal' + i + '-' + j + 'JsonPipelineTitle';
                    header.innerHTML = "JSON Pipeline";
                    div4.appendChild(header);
                    var div4Button = document.createElement('button');
                    div4Button.setAttribute('type','button');
                    div4Button.className = "close";
                    div4Button.setAttribute('data-dismiss','modal');
                    div4Button.setAttribute('aria-label','Close');
                    var span = document.createElement('span');
                    span.setAttribute('aria-hidden', 'true');
                    span.innerHTML = "&times;";
                    div4Button.appendChild(span);
                    div4.appendChild(div4Button);
                    div3.appendChild(div4);

                    var div5 = document.createElement('div');
                    div5.className = "modal-body";
                    div5.id = 'div5' + i + '-' + j + 'JsonPipeline';
                    showPipeline(testcase[j].getElementsByTagName('pipelineJSON')[0].innerHTML, 'div5' + i + '-' + j + 'JsonPipeline');
                    div3.appendChild(div5);             

                    var div6 = document.createElement('div');
                    div6.className = "modal-footer";
                    var div6Button = document.createElement('button');
                    div6Button.setAttribute('type','button');
                    div6Button.className = "btn btn-secondary";
                    div6Button.setAttribute('data-dismiss','modal');
                    div6Button.innerHTML = "Close";
                    div6.appendChild(div6Button);         
                    div3.appendChild(div6);
                    div2.appendChild(div3);
                    div1.appendChild(div2);
                    td6.appendChild(div1);
                }
			}

			table.appendChild(tbody);
			div.appendChild(table);
			container.appendChild(div);
		}
		body.appendChild(container);
	}
}

function showPipeline(url, id) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {	
		var response = xhr.responseText.replace(/&/g, '&amp;')
               .replace(/</g, '&lt;')
               .replace(/>/g, '&gt;')
               .replace(/"/g, '&quot;')
               .replace(/'/g, '&apos;');
		
        document.getElementById(id).innerHTML = response;
    };
    xhr.open('GET', url);
    xhr.send();
}


function clearTables()
{
    var summaryTableTbody = document.getElementById('summaryTableTbody');
	deleteRows(summaryTableTbody);
    var failureTableTbody = document.getElementById('failureTableTbody');
	deleteRows(failureTableTbody);
    var body = document.getElementById('body');
	var divToRemove = document.getElementById('container');
	divToRemove.remove();

}

function deleteRows(table)
{
    for (let i = table.rows.length - 1; i >= 0; i--)
        table.deleteRow(i);
}

function convertToZip()
{
    var date = window.location.pathname.split('/')[2];
    var htmlPage = document.getElementsByTagName('html')[0].outerHTML;
    var zipUrl = '/rest/svg.test.resources.zipReport';
	
	request = {
		htmlPage : null,
        date: null
    }

	
    request.htmlPage = htmlPage;
	request.date = date;

    $.ajax({
        type: "POST",
        crossDomain: true,
        url: zipUrl,
        data: 'jsonString=' + encodeURIComponent(JSON.stringify(request)),
        success: (response) => {
            //console.log(response.URL);
            var downloadBtn = document.getElementById("downloadZip");
            downloadBtn.style.visibility = "visible";
			
            downloadBtn.href = 'http://'  + response.URL;
            
            downloadBtn.addEventListener('click', function() {
				var downloadBtn = document.getElementById("downloadZip");
                downloadBtn.style.visibility = "hidden";
            });
            

        },
        error: function (xhr, textStatus, error, data) {
            console.log(xhr)
            console.log(xhr.responseText);
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
            console.log(data)
        }
    });
    
}

function generateDocument()
{
    var generateUrl = '/rest/svg.test.resources.generateDocx';
	var date = window.location.pathname.split('/')[2];
	
    request = {
        date: null
    }

    request.date = date;

    $.ajax({
        type: "POST",
        crossDomain: true,
        url: generateUrl,
        data: 'jsonString=' + JSON.stringify(request),
        success: (response) => {
            
            var downloadBtn = document.getElementById("downloadDocument");
            downloadBtn.style.visibility = "visible";
			
            downloadBtn.href = 'http://'  + response.URL;
            
            downloadBtn.addEventListener('click', function() {
				var downloadBtn = document.getElementById("downloadDocument");
                downloadBtn.style.visibility = "hidden";
            });
            

        },
        error: function (xhr, textStatus, error, data) {
            console.log(xhr)
            console.log(xhr.responseText);
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
            console.log(data)
        }
    });
}

function transformDate(date)
{
    var correctDate = date.substr(0, 4)
               + '-' + date.substr(4, 2)
               + '-' + date.substr(6, 2)
               + ' ' + date.substr(8, 2)
               + ':' + date.substr(10, 2)
               + ':' + date.substr(12, 2);
    
	return correctDate;
}

